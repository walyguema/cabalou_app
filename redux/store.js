import { configureStore } from '@reduxjs/toolkit';
import rootReducer from './reducers/rootReducer';
import storage from "@react-native-async-storage/async-storage";
import {persistStore,persistReducer,FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER} from "redux-persist";

const persistConfig = {
  key : "root",
  storage : storage,
  version : 1
}
const persistRedux = persistReducer(persistConfig,rootReducer);

export const store = configureStore({
  reducer : persistRedux,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistedStore = persistStore(store);
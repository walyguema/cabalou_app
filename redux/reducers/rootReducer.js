import { combineReducers } from '@reduxjs/toolkit';
import { countSlice } from '../actions/counterSlice';
import {userSlice} from '../actions/userSlice';
import { swiperSlice } from '../actions/swiperSlice';
import { authSlice } from '../actions/authSlicer';

const rootReducer = combineReducers({
  counter: countSlice.reducer,
  user : userSlice.reducer,
  swiper : swiperSlice.reducer,
  auth : authSlice.reducer
});

export default rootReducer;
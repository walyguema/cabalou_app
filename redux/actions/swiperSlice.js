import { createSlice } from "@reduxjs/toolkit";

export const swiperSlice = createSlice({
    name : "swiper",
    initialState : false,
    reducers : {
        isSee : (state,actions) =>{
            return actions.payload;
        },
        resetSwiper : (_,__) =>{
            return false;
        }
    }
});

export const {isSee,resetSwiper} = swiperSlice.actions;
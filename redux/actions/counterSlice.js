import { createSlice } from "@reduxjs/toolkit";

export const countSlice = createSlice({
  name : "counter",
  initialState : 0,
  reducers : {
    onCount : (state,action) =>{
      return state + action.payload
    },
    resetState : (state,action) =>{
      return 0;
    }
  }
});
export const {onCount,resetState} = countSlice.actions;
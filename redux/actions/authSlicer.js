import { createSlice } from "@reduxjs/toolkit";

export const authSlice = createSlice({
    name : "auth",
    initialState : {accessToken : null},
    reducers : {
        setLogin : (state, action) =>{
            state.accessToken = action.payload;
        },
        resetToken : (state,action) =>{
            state.accessToken = null;
        }
    }
})
export const {setLogin,resetToken} = authSlice.actions;
import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import {COLOR} from '../../utils/variables';
import ReservationItem from '../../components/Reservation/ReservationItem';
import ModalComponent from '../../components/Modal/ModalComponent';
import {getReservations} from '../../service/reservation';
import FlatView from '../../components/FlatView';
import NotUserDynamic from '../../components/requestRender/NotUserDynamic';
import Loader from '../../components/Loader';
import { useSelector } from 'react-redux';
import EmptyData from '../../components/requestRender/EmptyData';
import FloatButton from '../../components/FloatButton';
import { useNavigation } from '@react-navigation/native';

const Reservation = () => {
  const [searsh,setSearsh] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [reservations, setReservations] = useState([]);
  const [render, setRender] = useState(2);
  const [filter,setFilter] = useState("");
  const {accessToken} = useSelector(state => state.auth);
  const navigation = useNavigation();
  useEffect(() => {
    getReservations(filter)
    .then(res => {
        if (res.status == 200) {
          if(res.data.reservations.length){
            setReservations(res.data.reservations);
          }else{
            setReservations([]);
            setRender(0);
          }
        } else {
          setReservations([]);
          setRender(0);
          // console.log('====================================');
          // console.log(res.data.reservations);
          // console.log('====================================');
        }
      })
      .catch(err => {
        if(accessToken){
          setReservations([]);
          setRender(0);
        }else{
          setReservations([]);
          setRender(1);
        }
        // console.log('====================================');
        // console.log(res.data.reservations);
        // console.log('====================================');
      });
  }, [reservations]);
  const filterReservation = (newFilter) =>{
    setFilter(newFilter);
    setReservations([]);
    setRender(2);
    setSearsh("");
    setShowModal(false);
  }
  return (
    <>
      <ModalComponent
        style={style.containerModal}
        transparent={true}
        onClose={() => {
          setShowModal(false);
        }}
        visible={showModal}>
        <View style={style.containerFilter}>
          <TouchableOpacity onPress={() => {filterReservation("")}} style={style.buttonFilter}>
            <Text style={style.textFilter}>Toutes les réservations</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {filterReservation("next")}} style={style.buttonFilter}>
            <Text style={style.textFilter}>Réservations à venir.</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {filterReservation("progress")}} style={style.buttonFilter}>
            <Text style={style.textFilter}>Réservations en cours.</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {filterReservation("previous")}} style={style.buttonFilter}>
            <Text style={style.textFilter}>Réservations terminées.</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{...style.buttonFilter, borderBottomWidth: 0}}>
            <Text onPress={() => {filterReservation("canceled")}} style={style.textFilter}>Réservations annulées.</Text>
          </TouchableOpacity>
        </View>
      </ModalComponent>
      <View style={style.containerSearsh}>
        <TextInput placeholder="Recherche..." value={searsh} style={style.inputSearsh} onChangeText={(txt) =>{
          // setSeaesh
          setSearsh(txt);
        }}/>
        <TouchableOpacity
          style={style.containerIconFilter}
          onPress={() => {
            setShowModal(true);
          }}>
          <Icon name="sound-mix" color={COLOR.GLOBAL.mainColor} size={20} />
        </TouchableOpacity>
      </View>
      <FlatView
        data={reservations.filter((item) => item.property.name.toLowerCase().match(searsh.toLowerCase()))}
        emptyComponent={[<EmptyData/>,<NotUserDynamic />, <Loader />][render]}
        renderView={(item, i, _) => (
          <>
            <ReservationItem reservation={item} key={i} />
          </>
        )}
      />
      <FloatButton onClick={() =>{
        navigation.navigate("AddProperty");
      }}/>
    </>
  );
};
const style = StyleSheet.create({
  scrollContainer: {
    backgroundColor: '#fff',
    padding: 10,
  },
  containerSearsh: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingVertical: 20,
    gap: 8,
  },
  inputSearsh: {
    borderWidth: 1,
    flex: 1,
    borderRadius: 8,
    backgroundColor: '#fff',
    color: '#000',
    paddingHorizontal: 10,
    borderColor: '#ddd',
  },
  containerIconFilter: {
    backgroundColor: 'transparent',
    width: 50,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#eee',
  },
  containerModal: {
    paddingTop: 80,
    paddingHorizontal: 10,
    backgroundColor: 'rgba(0,0,0,0.3)',
    flex: 1,
  },
  containerFilter: {
    backgroundColor: '#fff',
    width: '100%',
    // padding : 10,
    borderRadius: 10,
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  buttonFilter: {
    paddingVertical: 15,
    paddingHorizontal: 5,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  textFilter: {
    color: '#000',
  },
});
export default Reservation;

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Home from "./home/Index";
import Reservation from "./reservation/Index";
import Favoris from "./favoris/Index";
import Messagerie from "./messagerie/Index";
import Profil from "./profil/Index";
import { ASSETS } from "../utils/assets";
import IconTabBar from "../components/IconTabBar";
import { StyleSheet } from "react-native";

const Tab = createBottomTabNavigator();

const BottomNavPages = () => {
    return (
        <Tab.Navigator initialRouteName='Home' screenOptions={{tabBarStyle : style.tabBarStyle,tabBarShowLabel : false,tabBarBadgeStyle : {backgroundColor : "purple"}}}>
            <Tab.Screen name='Home' component={Home} options={{tabBarIcon : (focus) => <IconTabBar text="Accueil" image={ASSETS.ICONSTAB.home} focus={focus.focused}/>,headerShown : false}}/>
            <Tab.Screen name='Reservation' component={Reservation} options={{tabBarIcon : (focus) => <IconTabBar text="Reservation" image={ASSETS.ICONSTAB.reservation} focus={focus.focused}/>}}/>
            <Tab.Screen name='Favoris' component={Favoris} options={{tabBarIcon : (focus) => <IconTabBar text="Favoris" image={ASSETS.ICONSTAB.favoris} focus={focus.focused}/>}}/>
            <Tab.Screen name='Messagerie' component={Messagerie} options={{tabBarIcon : (focus) => <IconTabBar text="Message" image={ASSETS.ICONSTAB.message} focus={focus.focused}/>}}/>
            <Tab.Screen name='Profil' component={Profil} options={{headerShown : false,tabBarIcon : (focus) => <IconTabBar text="Profil" image={ASSETS.ICONSTAB.profil} focus={focus.focused}/>}}/>
        </Tab.Navigator>
    );
}
const style = StyleSheet.create({
    tabBarStyle : {
        height : 80,
        paddingHorizontal : 10,
        // backgroundColor : "red",
        // margin : 10
    }
})
export default BottomNavPages;
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import Hebergeur from './../../components/details/hebergeur';
import Description from '../../components/details/description';
import Caractéristiques from '../../components/details/caractéristiques';
import Conditions from '../../components/details/conditions';
import Disponibilite from '../../components/details/disponibilite';
import Questions from '../../components/details/questions';
import Commentaires from '../../components/details/commentaires';
import BoutonReserver from '../../components/details/boutonReserver';
import {useNavigation, useRoute} from '@react-navigation/native';
import {getProperty} from '../../service/api';
import Rating from '../../components/Rating';
import {placeholer} from '../../utils/functions';
import Map from '../../components/Map/Map';

const DetailsReservation = () => {
  const [property, setProperty] = useState(null);
  const {uuid} = useRoute().params;
  const navigation = useNavigation();
  // console.log(uuid)

  useEffect(() => {
    getProperty({property_id: uuid}).then(res => {
      if (res.status == 200) {
        setProperty(res.data.property);
      }
    });
  }, []);

  return (
    <>
      <ScrollView style={styles.container}>
        <Text style={styles.textTitle}>{property?.name}</Text>
        <View style={styles.starsContainer}>
          <Rating note={property?.rating_avg} />
        </View>
        <View style={styles.locationContainer}>
          <Icon name="location-outline" size={20} color="#000" />
          <Text style={styles.locationcountry}>{property?.district}</Text>
        </View>

        <View style={styles.locationdescription}>
          {property?.main_components.map((value, index, array) => {
            return (
              <Text style={{color: '#000'}} key={index}>
                {value.size} {value.name}
              </Text>
            );
          })}
        </View>

        <View style={styles.locationButtons}>
          <TouchableOpacity style={styles.locationshare}>
            <Icon name="share-social-outline" size={20} color="#000" />
            <Text style={styles.locationShareText}>Partager</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.locationsave}>
            <Icon name="heart-outline" size={20} color="#000" />
            <Text style={styles.locationSaveText}>Enregistrer</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.locationImg}>
          <Image
            source={{uri: property?.images[0].path ?? placeholer(400, 300)}}
            style={styles.propertyImg}
          />
        </View>
        <View>
          <Hebergeur property={property} />
        </View>
        <View>
          <Description property={property} />
        </View>
        <View>
          <Caractéristiques property={property} />
        </View>
        <View>
          <Conditions property={property} />
        </View>
        <View>
          <Disponibilite />
        </View>
        <View style={styles.map}>
          <Text style={styles.titleMap}>Localisation</Text>
          <Text style={styles.descMap}>L'emplacement precis sera disponible apres la reservation.</Text>
          {property ? <Map longitude={property.google_lng} latitude={property.google_lat}/> : <Image source={{uri : placeholer(400,200)}}/>}
        </View>
        <View>
          <Questions />
        </View>
        <View>
          <Commentaires />
        </View>
      </ScrollView>
      <View>
        <BoutonReserver uuid={uuid} prices={property?.prices} />
      </View>
    </>
  );
};

export default DetailsReservation;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  textTitle: {
    fontSize: 20,
    padding: 20,
    fontWeight: 'bold',
    color: 'black',
  },
  starsContainer: {
    flexDirection: 'row',
    marginLeft: 14,
    position: 'relative',
    top: -6,
  },
  starText: {
    fontSize: 20,
  },
  locationContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 14,
  },
  locationcountry: {
    fontSize: 16,
    color: '#000',
  },
  locationdescription: {
    fontSize: 16,
    marginLeft: 16,
    marginTop: 10,
    color: 'black',
    flexDirection: 'row',
    gap: 10,
  },
  locationButtons: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 10,
    marginLeft: 16,
    gap: 10,
  },
  locationshare: {
    display: 'flex',
    flexDirection: 'row',
    gap: 10,
  },
  locationsave: {
    display: 'flex',
    flexDirection: 'row',
    gap: 10,
  },
  locationShareText: {
    textDecorationLine: 'underline',
    color: '#000',
  },
  locationSaveText: {
    textDecorationLine: 'underline',
    color: '#000',
  },
  locationImg: {
    marginTop: 20,
  },
  propertyImg: {
    width: '100%',
    height: 250,
  },
  map : {
    padding : 10
  },
  titleMap : {
    color : "#000",
    fontSize : 25,
    fontWeight : "bold"
  },
  descMap : {
    color : "#000",
    marginBottom : 20
  }
});

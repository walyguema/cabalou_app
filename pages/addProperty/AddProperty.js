import ProgressSteps, {
  Title,
  Content,
} from '@joaosousa/react-native-progress-steps';
import {useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import Lorem from '../../components/Lorem';
import VerticalView from '../../components/VerticalView';
import StapeContent from '../../components/stepContent';
import Informations from '../../components/formAddProperty/Informations';

const AddProperty = () => {
  const [step, setStep] = useState(0);
  return (
    <>
        <View style={style.container}>
        <ProgressSteps
          currentStep={step}
          orientation="horizontal"
          steps={[
            {
              id: 1,
              content: (
                 <Informations/>
              ),
            },
            {
              id: 2,
              content: (
                <Content>
                  <Lorem />
                  <Lorem />
                </Content>
              ),
            },
            {
              id: 3,
              content: (
                <Content>
                  <Lorem />
                  <Lorem />
                </Content>
              ),
            },
            {
              id: 4,
              content: (
                <Content>
                  <Lorem />
                  <Lorem />
                </Content>
              ),
            },
            {
              id: 5,
              content: (
                <Content>
                  <Lorem />
                  <Lorem />
                </Content>
              ),
            },
            {
              id: 6,
              content: (
                <Content>
                  <Lorem />
                  <Lorem />
                </Content>
              ),
            },
          ]}
        />
        <View style={{height : 80,backgroundColor : "#eee"}}>
          <Text>Footer</Text>
        </View>
        </View>
    </>
  );
};
const style = StyleSheet.create({
  container : {
    backgroundColor : "#fff",
    flex : 1
  }
})
export default AddProperty;
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from "@react-navigation/native";
import React from 'react';
import BottomNavPages from './BottomNavPages';
import DetailsReservation from './details/DetailsReservation';
import TestsRedux from './testsRedux/TestsRedux';
import SwiperPage from './swiper/SwiperPage';
import Paiement from './Paiement/Index';
import { useSelector } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';
import AddProperty from './addProperty/AddProperty';
const Stack = createNativeStackNavigator();
const Root = () => {
    const swiper = useSelector(state => state.swiper);
    return (
        <>
            <NavigationContainer>
      <Stack.Navigator initialRouteName={swiper ? "BottomNavPages" : 'Swiper'}>
        <Stack.Screen name='BottomNavPages' component={BottomNavPages} options={{headerShown : false}}/>
        <Stack.Screen name='detailsReservation' options={{
          headerTitle: () => (
            <View style={styles.headerTitle}>
                <Text style={styles.headerTitle2}>Demande de réservation</Text>
            </View>
        ),
        }}  component={DetailsReservation} />
        <Stack.Screen name='Paiement' options={{headerTitle : "Demande de réservation"}} component={Paiement}/>
        <Stack.Screen name='Tests' component={TestsRedux}/>
        <Stack.Screen name='Swiper' component={SwiperPage} options={{headerShown : false}}/>
        <Stack.Screen name='AddProperty' component={AddProperty} options={{headerShadowVisible : false,headerTitle : "Nouvelle reservation"}}/>
      </Stack.Navigator>
    </NavigationContainer>
        </>
    );
}
const styles = StyleSheet.create({
    headerTitle: {
        marginHorizontal: 10, 
        marginVertical: 5,
        marginLeft: 70,
    },
    headerTitle2: {
      fontSize: 20,
      color: 'black',
      fontWeight: 'bold',
    },
  });
export default Root;
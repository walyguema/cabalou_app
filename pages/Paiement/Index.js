import React, {useEffect, useState} from 'react';
import {
  Image,
  Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ContainerShadow from '../../components/ContainerShadow';
import {useRoute} from '@react-navigation/native';
import {getProperty} from '../../service/api';
import Loader from '../../components/Loader';
import {COLOR, formatDate, screenHeight} from '../../utils/variables';
import Rating from '../../components/Rating';
import Rows from '../../components/Rows';
import { useSelector } from 'react-redux';
import AuthUser from '../../components/auth';
import PaiementComponent from '../../components/Paiement/PaiementComponent';
import ReservationForm from '../../components/Reservation/ReservationForm';
import { placeholer } from '../../utils/functions';

const Paiement = () => {
  const user = useSelector((state) => state.user);
  const [property, setProperty] = useState(null);
  const [start_periode,setStart_periode] = useState({});
  const [end_date,setEnd_periode] = useState({});
  const {uuid} = useRoute().params;
  
  useEffect(() => {
    getProperty({property_id: uuid}).then(res => {
      if (res.status == 200) {
        setStart_periode(formatDate(res.data.property.default_periode.start_date));
        setEnd_periode(formatDate(res.data.property.default_periode.end_date));
        setProperty(res.data.property);
      }
    });
  }, []);
  return (
    <SafeAreaView>
      <ScrollView
        style={{
          backgroundColor: '#FAFAFA',
          paddingHorizontal: 10,
        //   minHeight: screenHeight,
        }}>
        {property ? (
          <>
            <ContainerShadow style={style.container}>
              <View style={style.contentImg}>
                <Image
                  style={style.propertyImg}
                  source={{
                    uri: property
                      ? property.images[0].path
                      : placeholer(200,200),
                  }}
                />
              </View>
              <View style={style.containerText}>
                <Text style={style.textProperty}>{property?.name}</Text>
                <Rating note={property.ratings.length} />
                <Text style={style.textDetail}>
                  {property.country?.name}, {property?.city},{' '}
                  {property?.district}
                </Text>
                <View style={style.containerComponent}>
                  {property.main_components.map((it, i, _) => (
                    <Text style={style.textDetail} key={i}>
                      {it.size} {it.name}
                    </Text>
                  ))}
                </View>
              </View>
            </ContainerShadow>
            {/*  */}
            <ContainerShadow style={{gap: 6}}>
              <Rows style={style.rowBorder}>
                <Text style={style.textRows}>{property.prices[0].amount.toLocaleString()} {property.prices[0].currency} x {property.prices[0].max_duration} nuitée</Text>
                <Text style={style.textRows}>{parseInt(property.prices[0].amount/6).toLocaleString()} {property.prices[0].currency}</Text>
              </Rows>
              <Rows>
                <Text style={style.textRows}>Frais de service :</Text>
                <Text style={style.textRows}>{property.prices[0].service_fee.toLocaleString()} {property.prices[0].currency}</Text>
              </Rows>
              <Rows>
                <Text style={style.textRows}>Total Taxes comprises : </Text>
                <Text style={style.textRows}>{property.prices[0].ttc_amount.toLocaleString()} {property.prices[0].currency}</Text>
              </Rows>
            </ContainerShadow>
            {/*  */}
            <ContainerShadow>
                <Rows>
                    <Text style={style.textRows}>Date</Text>
                    <Text style={style.textRows}>{start_periode.day} {start_periode.date} {start_periode.month} {start_periode.year} - {end_date.day} {end_date.date} {end_date.month} {end_date.year}</Text>
                </Rows>
                <Rows>
                    <Text style={style.textRows}>Visiteurs</Text>
                    <Text style={style.textRows}>{property.guest_adult} adulte(s), {property.guest_child} enfant(s)</Text>
                </Rows>
                <TouchableOpacity style={style.buttonSet}>
                    <Text style={{color : "#888"}}>Modifier</Text>
                </TouchableOpacity>
            </ContainerShadow>
            {/*  */}
            {user ? property.auto_booking ? <PaiementComponent/> : <ReservationForm/> : <AuthUser/>}
            
          </>
        ) : (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              minHeight: screenHeight,
            }}>
            <Loader />
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  contentImg: {
    position: 'relative',
    flex: 1,
    overflow: 'hidden',
    borderRadius: 4,
  },
  propertyImg: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    left: 0,
    top: 0,
  },
  containerText: {
    flex: 2,
    gap: 3,
  },
  container: {
    flexDirection: 'row',
    gap: 8,
    minHeight: 120,
  },
  containerComponent: {
    flexDirection: 'row',
    gap: 5,
  },
  textProperty: {
    fontSize: 18,
    color: '#000',
  },
  rowBorder: {
    borderBottomWidth: 2,
    borderBottomColor: '#ddd',
  },
  textRows : {
    color : "#000",
    fontSize : 14.5
  },
  textDetail : {
    color : "#000"
  },
  buttonSet : {
    width : "100%",
    marginTop : 10,
    backgroundColor : "#f1f1f1",//COLOR.GLOBAL.buttonSec,
    // justifyContent : "center",
    alignItems : "center",
    paddingVertical : 8,
    borderWidth : 1,
    borderColor : "#ddd",
  },
});
export default Paiement;

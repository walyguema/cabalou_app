import { ScrollView, StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import UserProfil from "../../components/profilComponent/UserProfil";
import ButtonIcone from "../../components/buttons/ButtonIcone";
import { ASSETS } from "../../utils/assets";
import { logoutUser } from "../../redux/actions/userSlice";
import NotUser from "../../components/requestRender/NotUser";
import { resetToken } from "../../redux/actions/authSlicer";

const Profil = () => {
    const user = useSelector(state => state.user);
    const dispatch = useDispatch();
    const logout = () =>{
        dispatch(logoutUser());
        dispatch(resetToken());
    }
    return (
        <>
        <ScrollView style={{backgroundColor : "#fff"}}>
        <View style={style.container}>
        {user ? <UserProfil user={user}/> : <NotUser/>}
        <View style={style.containerButtons}>
            {user ? <ButtonIcone icon={ASSETS.ICONSTAB.profil} text="Mon compte"/> : null}
            <ButtonIcone icon={ASSETS.GLOBAL.translate} text="Langues"/>
            <ButtonIcone icon={ASSETS.GLOBAL.confidentialite} text="Politique de confidentialite"/>
            <ButtonIcone icon={ASSETS.SEARSH.recherche} text="Aide"/>
            <ButtonIcone icon={ASSETS.GLOBAL.logout} onPress={logout} text="Deconnexion"/>
        </View>
        </View>
        </ScrollView>
        </>
    );
}
const style = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : "#fff"
    },
    profil : {
        width : 100,
        height : 100,
        backgroundColor : "#f1f1f1",
        borderRadius : 200
    },
    containerButtons : {
        paddingHorizontal : 10,
        marginTop : 20
    }
})
export default Profil;
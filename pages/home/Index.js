import React, { useEffect, useState } from "react"
import { FlatList, SafeAreaView, ScrollView, StyleSheet, Text, View } from "react-native"
import Input from "../../components/inputs/Input"
import { ASSETS } from "../../utils/assets"
import HorizontalView from "../../components/HorizontalView"
import CategorieItem from "../../components/CategorieItem"
import Property from "../../components/Property/Property"
import Loader from "../../components/Loader"
import { getAllProperty } from "../../service/api"
import EmptyData from "../../components/requestRender/EmptyData"
import { useSelector } from "react-redux"

const Home = () => {
    // const {accessToken} = useSelector(state => state.auth);
    // console.log('\n===============accessToken=====================\n');
    // console.log(accessToken);
    // console.log('\n=================accessToken===================\n');
    const [data,setData] = useState([]);
    const [length,setLength] = useState(4);
    const [loading,setLoading] = useState(true);
    useEffect(() =>{
        getAllProperty({size : length}).then(res =>{
            if(res.status == 200){
                if(res.data.properties.length){
                    setData(res.data.properties);
                }else{
                    setData([]);
                    setLoading(false);
                }
            }else{
                setData([]);;
                setLoading(false);
            }
            }).catch(err =>{
            setData([]);
            setLoading(false);
        })
    },[length])
    return (
        <SafeAreaView style={{ backgroundColor: "#fff", flex: 1 }}>
            <View style={style.header}>
                <View style={{ paddingVertical : 20,paddingHorizontal : 13 }}>
                    <Input placeholder="Recherche..." icon={ASSETS.SEARSH.recherche} />
                </View>
                <View>
                    <HorizontalView style={{backgroundColor : "#fff"}}>
                        <CategorieItem text="Tout" active={true} icon={ASSETS.CATEGORIES.all}/>
                        <CategorieItem text="Piscine" icon={ASSETS.CATEGORIES.piscine}/>
                        <CategorieItem text="Plage" icon={ASSETS.CATEGORIES.plage}/>
                        <CategorieItem text="Foret" icon={ASSETS.CATEGORIES.foret}/>
                        <CategorieItem text="Immeuble" icon={ASSETS.CATEGORIES.immeuble}/>
                        <CategorieItem text="Luxieux" icon={ASSETS.CATEGORIES.luxe}/>
                    </HorizontalView>
                </View>
            </View>
            <FlatList
            data={data.filter((_,i,__) => i <= length)}
            onEndReachedThreshold={0.4}
            renderItem={({item,index,sepators}) => (
                <Property key={index} property={item}/>
            )}
            ListFooterComponent={!data.length && !loading ? <EmptyData/> : <Loader/>}
            onEndReached={() =>{
                setLength(length + 1);
            }}
            />
        </SafeAreaView>
    )
}
const style = StyleSheet.create({
    header: {
        backgroundColor: "#fff",
        shadowColor: "#999",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.22,
        shadowRadius: 9.22,
        elevation: 12
    }
})
export default Home;
import React from 'react';
import { StatusBar, StyleSheet } from 'react-native';
import {Provider} from "react-redux";
import { persistedStore, store } from '../redux/store';
import { PersistGate } from 'redux-persist/integration/react';
import Root from './Root';

function App(): React.JSX.Element {
  return (
    <>
    <Provider store={store}>
      <PersistGate persistor={persistedStore}>

    <StatusBar barStyle={"dark-content"} backgroundColor="#fff"/>
    <Root/>
    </PersistGate>  
    </Provider>
    </>
  );
}

export default App;
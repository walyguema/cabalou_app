import React, { useState } from 'react';
import Swiper from 'react-native-swiper';
import SwiperScreen from '../../components/swiper/SwiperScreen';
import { ASSETS } from '../../utils/assets';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { isSee } from '../../redux/actions/swiperSlice';
const SwiperPage = () => {
    const [index,setIndex] = useState(0);
    const dispatch = useDispatch();
    const navigate = useNavigation();
    const gotTo = () =>{
        navigate.navigate("BottomNavPages");
        dispatch(isSee(true));
    }
    return (
        <>
            <Swiper>
                <SwiperScreen onClick={() => {gotTo()}} img={ASSETS.SWIPER.step2} title="Votre Maison Temporaire" desc="Des Locations Meublées Exceptionnelles pour une Expérience Inoubliable."/>
            </Swiper>
        </>
    );
}
export default SwiperPage;
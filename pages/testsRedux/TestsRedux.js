import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from 'react-redux';
import { onCount, resetState } from '../../redux/actions/counterSlice';

const TestsRedux = () => {
    const count = useSelector((state) => state.counter);
    const dispatch = useDispatch();

    const setStateCpt = (value) =>{
        dispatch(onCount(value));
    }
    const onReset = () =>{
        dispatch(resetState());
    }
    return (
        <View style={style.container}>
            <Text style={style.text}>{count}</Text>
            <View style={style.containerButton}>
                <TouchableOpacity onPress={() => {setStateCpt(-1)}} style={style.buttons}>
                    <Text style={style.textButton}>-</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() =>{setStateCpt(1)}} style={style.buttons}>
                    <Text style={style.textButton}>+</Text>
                </TouchableOpacity>
            </View>
            <View style={style.containerButton}>
                <TouchableOpacity style={style.buttons} onPress={onReset}>
                    <Text style={style.textButton}>RESET</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}
const style = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : "#fff",
        gap : 30,
        justifyContent : "center",
        alignItems : "center"
    },
    text : {
        fontSize : 60,
        color : "#000",
        fontWeight : "bold"
    },
    containerButton : {
        flexDirection : "row",
        height : 50,
        gap : 10,
        paddingHorizontal : 10
    },
    buttons : {
        flex : 1,
        backgroundColor : "royalblue",
        justifyContent : "center",
        alignItems : "center"
    },
    textButton : {
        color : "#fff",
        fontSize : 20
    }
})
export default TestsRedux;
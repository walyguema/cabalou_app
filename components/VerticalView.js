import { ScrollView, Text } from "react-native";
import { parseChildrenToArray } from "./stepContent/utils/utils";

const VerticalView = ({style,children}) => {
    return (
        <>
        <ScrollView showsVerticalScrollIndicator={false} style={style}>
        {children}
        </ScrollView>
        </>
    );
}
export default VerticalView;
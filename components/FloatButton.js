import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { addShadow } from "../utils/functions";

const FloatButton = ({onClick}) => {
    return (
        <>
            <TouchableOpacity style={style.container} onPress={() =>{
                if(onClick){
                    onClick();
                }
            }}>
                <Text style={style.text}>+</Text>
            </TouchableOpacity>
        </>
    );
}

const style = StyleSheet.create({
    container : {
        width : 60,
        height : 60,
        backgroundColor : "royalblue",
        position : "absolute",
        right : 10,
        bottom : 10,
        borderRadius : 99,
        justifyContent : "center",
        alignItems : "center",
        ...addShadow()
    },
    text : {
        color : "#fff",
        fontSize : 35
    }
})
export default FloatButton;
import React, { useState } from 'react';
import { Modal,Text, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ModalComponent = ({visible,transparent = false,style = {},onClose,children}) => {
    return (
        <>
            <Modal transparent={transparent} visible={visible} animationType='slide'>
            <View style={style}>
                <TouchableOpacity onPress={() =>{
                    if(onClose){
                        onClose();
                    }
                }} style={mainStyle.containerClose}>
                    <Icon name="close" color="#000" size={20}/>
                </TouchableOpacity>
                <ScrollView>
                    {children}
                </ScrollView>
            </View>
            </Modal>
        </>
    );
}
const mainStyle = StyleSheet.create({
    containerClose : {
        position : "absolute",
        backgroundColor : "#fff",
        right : 10,
        top : 10,
        borderRadius : 99,
        zIndex : 100,
        padding : 5,
        shadowColor: "#aaa",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.22,
        shadowRadius: 9.22,
        elevation: 12
    }
})
export default ModalComponent;
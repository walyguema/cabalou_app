import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {addShadow} from '../utils/functions';

const ClicableContainerShadow = ({style,children}) => {
  return (
    <TouchableOpacity style={{...mainStyle.container,...style}}>{children}</TouchableOpacity>
  );
};

const mainStyle = StyleSheet.create({
  container: {
    padding: 10,
    marginVertical : 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    borderWidth: 1,
    borderRadius : 8,
    borderColor: '#eee',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.18,
    shadowRadius: 4.59,
    elevation: 5,
  },
});

export default ClicableContainerShadow;

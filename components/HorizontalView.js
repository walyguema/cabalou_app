import React from 'react';
import { ScrollView } from "react-native";

const HorizontalView = ({style,children}) => {
    return (
        <ScrollView style={{...style}} showsHorizontalScrollIndicator={false} horizontal={true}>
            {children}
        </ScrollView>
    );
}

export default HorizontalView;
import React from "react";
import { StyleSheet, View } from "react-native";
/**
 * 
 * @param {{style : StyleSheet,children : React.JSX.Element}} param
 * @returns 
 */
const Rows = ({style,children}) => {
    return (
        <View style={{...mainStyle.container,...style}}>
            {children}
        </View>
    );
}
const mainStyle = StyleSheet.create({
    container : {
        width : "100%",
        flexDirection : "row",
        justifyContent : "space-between",
        // backgroundColor : "orange",
        paddingVertical : 5
    }
})
export default Rows;
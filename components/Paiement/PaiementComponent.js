import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ASSETS} from '../../utils/assets';
import InputSelect from '../inputs/InputSelect';
import ButtonLoad from '../buttons/ButtonLoad';
import InputPhoneNumber from '../inputs/InputPhoneNumber';
import {getCountryForPayment} from '../../service/country';
import {getPaymentOptions} from '../../service/payment';
const PaiementComponent = () => {
  const [group, setGroup] = useState('MOBILE');
  const [selectedMomo, SetSelectedMomo] = useState(null);
  const [pays, setPays] = useState({});
  const [items, setItems] = useState([]);
  const [paymentOption, setPayementOption] = useState([]);

  useEffect(() => {
    getCountryForPayment().then(res => {
      setItems(res.countries);
    });
  }, []);
  useEffect(() => {
    getPaymentOptions().then(res => {
      setPayementOption(res.payment_options);
    });
  }, []);
  const onSelectItem = selectPays => {
    SetSelectedMomo(null);
    setGroup("MOBILE");
    setPays(selectPays);
    // setMomo();
  };

  const onSelectMomo = momo => {
    SetSelectedMomo(momo);
    setGroup(momo.group);
  };
  return (
    <View style={style.container}>
      {/*  */}
      <Text style={style.title}>Comment souhaitez-vous payer ?</Text>
      {/*  */}
      <View style={style.containerSec}>
        <Image style={style.iconSec} source={ASSETS.GLOBAL.lock} />
        <Text style={style.textSec}>Reservation secueisee par Cabalou</Text>
      </View>
      {/*  */}
      <InputSelect
        items={items}
        onSelectItem={it => {
          onSelectItem(it);
        }}
      />
      <View style={style.conatainerMomo}>
        {pays.payment_options
          ? paymentOption
              .filter((val, i, _) =>
                `${pays.payment_options}`.split(',').includes(`${val.id}`),
              )
              .map((item, i, _) => (
                <TouchableOpacity
                  onPress={() => {
                    onSelectMomo(item);
                  }}
                  key={i}
                  style={{
                    ...style.momo,
                    borderWidth: 2,
                    borderColor: selectedMomo == item ? 'green' : '#888',
                  }}>
                  <Image
                    style={{width: 25, height: 25}}
                    source={{uri: item.image}}
                  />
                </TouchableOpacity>
              ))
          : null}
        <View
          style={{
            width: '100%',
            marginVertical: 10,
            display: selectedMomo && group == 'MOBILE' ? 'flex' : 'none',
          }}>
          <InputPhoneNumber defaultCode={pays.iso_code} />
        </View>
      </View>
      {/*  */}
      <View
        style={{
          ...style.containerSec,
          backgroundColor: 'rgba(36, 161, 91, 0.2)',
          borderColor: '#aaa',
          borderWidth: 1,
          display : group == "BANK" ? "flex" : "none"
        }}>
        <Text
          style={{
            ...style.textSec,
            textAlign: 'center',
            width: '100%',
            color: '#000',
          }}>
          Vous seriez redirigé sur la platforme de notre partenaire.
        </Text>
      </View>
      {/*  */}
      <ButtonLoad texte="Payer 165 000 FRCFA" />
    </View>
  );
};
const style = StyleSheet.create({
  container: {
    marginVertical: 30,
    gap: 20,
  },
  title: {
    color: '#000',
    fontSize: 23,
    fontWeight: 'bold',
  },
  containerSec: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#EFF3F9',
    paddingHorizontal: 10,
    height: 45,
    gap: 10,
    borderRadius: 5,
    borderWidth: 2,
    borderColor: '#eee',
  },
  iconSec: {
    width: 20,
    height: 20,
    tintColor: '#aaa',
  },
  textSec: {
    color: '#000',
    fontSize: 14,
  },
  conatainerMomo: {
    // backgroundColor : "orange",
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 10,
    flexWrap: 'wrap',
  },
  momo: {
    backgroundColor: '#fff',
    width: 90,
    height: 45,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.18,
    shadowRadius: 4.59,
    elevation: 2,
  },
});
export default PaiementComponent;

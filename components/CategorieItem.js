import React from 'react';
import { Button, Image, Pressable, StyleSheet, Text, View } from "react-native";

import { ASSETS } from '../utils/assets';
import { COLOR, screenWidth } from '../utils/variables';
/**
 * 
 * @param {{text : string, icon : NodeRequire,active : boolean}} param
 * @returns 
 */
const CategorieItem = ({text,icon,active = false}) => {
    return (
        <Pressable style={style.container}>
            <View style={{...style.itemContainer,borderBottomWidth : active ? 3 : 0,borderColor : COLOR.GLOBAL.item}}>
                <Image source={icon} style={style.itemImage}/>
                <Text style={style.text}>{text}</Text>
            </View>
        </Pressable>
    );
}
const style = StyleSheet.create({
    container : {
    },
    itemContainer : {
        width : screenWidth/5,
        justifyContent : "center",
        alignItems : "center",
        gap : 5,
        // marginVertical : 6,
        paddingVertical : 10
    },
    itemImage : {
        objectFit : "contain",
        height : 23,
        width : 25,
        tintColor : COLOR.GLOBAL.item
    },
    text : {
        fontSize : 13,
        color : COLOR.GLOBAL.item
    }
})
export default CategorieItem;
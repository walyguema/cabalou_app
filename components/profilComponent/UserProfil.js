import React from 'react';
import { Image, StyleSheet, Text, View } from "react-native";
import { ASSETS } from '../../utils/assets';

const UserProfil = ({user}) => {
    return (
        <View style={style.container}>
            <View style={style.profil}>
                <Image style={style.imgProfil} source={ASSETS.GLOBAL.user_empty}/>
            </View>
            <View style={style.containerText}>
                <Text style={style.name}>{user.first_name} {user.last_name}</Text>
                <Text style={style.email}>{user?.email}</Text>
                <View style={style.containerVerify}>
                    <Text style={style.textVerify}>{user.identity_verification_status}</Text>
                </View>
            </View>
        </View>
    );
}
const style = StyleSheet.create({
    container : {
        marginTop : 30,
        paddingVertical : 20,
        paddingHorizontal : 10,
        flexDirection : "row",
        alignItems : "center",
        justifyContent : "flex-start",
        gap : 10,
        // borderBottomWidth : 2,
        // borderColor : "#eee"
    },
    profil : {
        width : 80,
        height : 80,
        backgroundColor : "#f1f1f1",
        borderRadius : 200
    },
    imgProfil : {
        width : "100%",
        height : "100%",
        objectFit : "cover",
        borderRadius : 99
    },
    containerText : {
        // gap : 2
    },
    name : {
        color : "#000",
        fontSize : 22
    },
    email : {
        color : "#888",
        fontSize : 15
    },
    containerVerify : {
        width : "auto",
        flexDirection : "row"
    },
    textVerify : {
        backgroundColor : "green",
        paddingHorizontal : 20,
        paddingVertical : 4,
        borderRadius : 40,
        color : "#fff",
        marginTop : 4,
        fontSize : 10
    }
})
export default UserProfil;
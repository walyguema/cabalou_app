import { StyleSheet, Text, View } from "react-native";
import HorizontalView from "../HorizontalView";
import { screenHeight, screenWidth } from "../../utils/variables";
import VerticalView from "../VerticalView";
import { parseChildrenToArray } from "./utils/utils";
import StepeItem from "./components/StepeItem";
import React from "react";

/**
 * 
 * @param {{labels : [string],children : React.JSX.Element}} param0 
 * @returns 
 */
const StapeContent = ({labels = [],children}) => {
    const items = parseChildrenToArray(children);
    return (
        <>
            <View style={{position : "relative"}}>
            <HorizontalView style={{backgroundColor : "#eee",position : "absolute",zIndex : 10,height : 70}}>
                <View style={style.container}>
                {items.map((__,i,_) =>{
                    return <StepeItem label={labels[i]} stepe={i + 1} key={i}/>
                })}
                </View>
            </HorizontalView>
            </View>
            <VerticalView style={{backgroundColor : "gold"}}>
                <View style={{height : 70}}></View>
                {children}
            </VerticalView>
        </>
    );
}
const style = StyleSheet.create({
    container : {
        // height : 70,
        // paddingVertical : 10,
        // backgroundColor : "orange",
        minWidth : screenWidth,
        justifyContent : "space-between",
        alignItems : "center",
        // gap : 20,
        flexDirection : "row"
    }
})
export default StapeContent;
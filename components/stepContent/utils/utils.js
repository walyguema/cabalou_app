import React from "react";

/**
 * 
 * @param {[React.ReactComponentElement]} children 
 * @returns 
 */
export function parseChildrenToArray(children){
    try {
        return [...children]
    } catch (error) {
        return children ? [children] : [];
    }
}
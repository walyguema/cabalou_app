import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const StepeItem = ({stepe,label}) => {
    return (
        <>
            <TouchableOpacity style={style.container}>
                <View style={style.stepe}>
                    <Text style={style.stepeText}>{stepe}</Text>
                </View>
                {label ? <Text>{label}</Text> : null}
            </TouchableOpacity>
        </>
    );
}
const style = StyleSheet.create({
    container : {
        backgroundColor : "#aaa",
        justifyContent : "center",
        alignItems : "center",
        flex : 1,
        gap : 6
    },
    stepe : {
        width : 34,
        height : 34,
        backgroundColor : "#fff",
        borderRadius : 99,
        justifyContent : "center",
        alignItems : "center",
        borderWidth : 4
    },
    stepeText : {
        fontSize : 15
    }
})
export default StepeItem;
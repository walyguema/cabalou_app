import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { ASSETS } from '../utils/assets';
const Rating = ({note = 0}) => {
    let ratingComponent = [];
    for(let i = 1; i <= 5; i++){
        ratingComponent.push(<Image key={i} style={{...style.star,tintColor : i <= note ? "gold" : "#aaa"}} source={ASSETS.GLOBAL.star}/>)
    }
    return <View style={style.containerStar}>
        {ratingComponent}
    </View>;
}
const style = StyleSheet.create({
    star : {
        width : 20,
        height : 20
    },
    containerStar : {
        flexDirection : "row"
    }
})
export default Rating;
import React from 'react';
import {Image, Pressable, StyleSheet, Text, View} from 'react-native';
import {COLOR, screenWidth} from '../../utils/variables';
import {ASSETS} from '../../utils/assets';
import { useNavigation } from '@react-navigation/native';

const Property = ({property}) => {
    const navigation = useNavigation();
  return (
    <>
      <Pressable style={style.container} onPress={() =>{navigation.navigate("detailsReservation",{uuid : property.uuid})}}>
        <View style={style.contentImg}>
          <Image style={style.image} source={{
            uri : property.images[0].path
          }} />
          <Pressable style={style.favoris}>
            <Image style={style.favorisIcn} source={ASSETS.PROPERTY.favoris} />
          </Pressable>
        </View>
        <View style={style.contentDetails}>
          
          {/* <View style={style.textNormal}>1 chambre, 1 li, 1 Salle de bain</View> */}
          <View style={{flexDirection: "row",gap : 14}}>
            {property.components.map((it,i,_) => (
              <View style={{flexDirection : "row",gap : 6,alignItems : "center"}} key={i}>
                <Image style={{width : 18,height : 18,objectFit : "contain",tintColor : COLOR.GLOBAL.item}} source={{
                  uri : it.icon
                }}/>
                <Text style={{...style.textNormal,color : COLOR.GLOBAL.item}}>{it.size}</Text>
              </View>
              ))}
          </View>
          <Text style={style.textBold}>{property.name}</Text>
          <Text style={style.textNormal}>{property.district}</Text>
          <View style={style.contentText}>
            <Text style={style.textBold}>{property.prices[0].amount.toLocaleString()}</Text>
            <Text style={style.textNormal}>FCFA/nuitée</Text>
          </View>
        </View>
      </Pressable>
    </>
  );
};
const style = StyleSheet.create({
  container: {
    // backgroundColor: 'orange',
    marginBottom: 4,
    width: screenWidth,
    maxWidth: 420,
    padding: 10,
  },
  image: {
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    borderRadius: 10,
  },
  contentImg: {
    width: '100%',
    height: 300,
    position: 'relative',
    marginBottom: 8,
  },
  favoris: {
    position: 'absolute',
    width: 25,
    height: 25,
    right: 10,
    top: 10,
  },
  favorisIcn: {
    width: '100%',
    height: '100%',
    objectFit: 'contain',
    tintColor: '#000',
    opacity: 0.7
  },
  textBold: {
    fontWeight: 'bold',
    color: '#000',
    fontSize: 18,
  },
  textNormal: {
    color: '#000',
  },
  contentText : {
    flexDirection : "row",
    alignItems : "flex-end",
    gap : 5
  },
  contentDetails : {
    gap : 4
  }
});
export default Property;

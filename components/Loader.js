import React from 'react';
import { ActivityIndicator, Text, View } from "react-native";
// import { View } from 'react-native-reanimated/lib/typescript/Animated';

const Loader = ({active = true}) => {
    return (
    active ? <ActivityIndicator size="large" color="royalblue"/> : null
    )
}

export default Loader;
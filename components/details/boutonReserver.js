import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'

const BoutonReserver = ({prices,uuid}) => {
    const navigation = useNavigation();
    return (
        <View style={styles.resevationContainer}>
            <View>
                <Text style={styles.resevationNuit}>32 000 Fcfa x 7 nuits</Text>
                <Text style={styles.resevationPrice}>{prices ? prices[0].amount.toLocaleString() : null} XAF</Text>
            </View>
            <View style={styles.resevationButton}>
                <TouchableOpacity onPress={() =>{
                    navigation.navigate("Paiement",{uuid : uuid})
                }}>
                    <Text style={styles.resevationButtonText}>Réserver</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default BoutonReserver

const styles = StyleSheet.create({
    resevationContainer: {
        borderTopColor: '#E8E8E8',
        borderTopWidth: 1,
        padding: 14,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    resevationNuit: {
        color: 'black',
        marginTop: 5,
    },
    resevationPrice: {
        fontWeight: 'bold',
        fontSize: 25,
        color: 'black',
        marginTop: 15,
    },
    resevationButton: {
        display: 'flex',
        width: 180,
        height: 60,
        padding: 8,
        marginTop: 10,
        borderRadius: 5,
        backgroundColor: '#0050CF',
        alignItems: 'center',
        justifyContent: 'center',
    },
    resevationButtonText: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
    }
})
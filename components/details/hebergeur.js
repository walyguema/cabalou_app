import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { ASSETS } from '../../utils/assets'
import Icon from 'react-native-vector-icons/Ionicons';
import { placeholer } from '../../utils/functions';

const Hebergeur = ({property}) => {
//  property?.host.photo
  return (
    <View style={styles.container} >
      <View>
        <Image
          source={{uri : property?.host.photo ?? placeholer()}}
          style={styles.hebergeurImg}
        />
        <View>
          <Text style={styles.hebergeurName}>
            {property?.host.first_name} {property?.host.last_name}
          </Text>
        </View>
        <View style={styles.hebergeurVerification} >
          <Icon name="checkmark-circle" size={20} color="#096237" />
          <Text style={styles.hebergeurStatus}  >
            {property?.host.identity_verified_at ? 'Hébergeur vérifié' : 'Hébergeur non vérifié'}
          </Text>
        </View>
        <TouchableOpacity style={styles.hebergeurContactBtn} >
          <Text style={styles.hebergeurContactText}>Contacter l'hébergeur</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Hebergeur

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    padding: 15,
  },
  hebergeurImg: {
    // display: 'flex',
    width: 70,
    height: 70,
    marginTop: 30,
    borderRadius : 999,
    objectFit : "fill"

  },
  hebergeurName: {
    display: 'flex',
    position: 'relative',
    top: -80,
    left: 80,
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',

  },
  hebergeurVerification: {
    width: 150,
    padding: 2,
    top: -75,
    left: 80,
    borderRadius: 5,
    flexDirection: 'row',
    backgroundColor: '#E9F9EF',
  },
  hebergeurStatus: {
    marginLeft: 5,
    fontWeight: 'bold',
    color: 'black',
  },
  hebergeurContactBtn: {
    display: 'flex',
    width: 200,
    top: -65,
    left: 80,
    padding: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#0050CF',
    alignItems: 'center',
  },
  hebergeurContactText: {
    color: '#0050CF',
    fontSize: 16,
    fontWeight: 'bold',
  },
})
import { Image, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { ASSETS } from '../../utils/assets'
import SvgUri from 'react-native-svg-uri';
import { useRoute } from '@react-navigation/native';
import { getProperty } from '../../service/api';
import { useSelector } from 'react-redux';
import { getComments } from '../../service/comments';

const Commentaires = () => {

    const [comments, setComments] = useState([]);
    const { uuid } = useRoute().params;

    useEffect(() => {
        getComments(uuid).then(res => {

            if (res.status = 200) {
                setComments(res.data.comments);
            } else {
                setComments([])
            }
        })
    }, []);

    return (
        <View style={styles.commentairesContainer}>
            <Text style={styles.commentairesText}>Commentaires</Text>
            <View style={styles.UserCommentaireContainer}>
                {comments?.map((value, index, array) => {
                    return (
                        <View key={index} style={styles.UserImg}>
                            <Image
                                source={{ uri: value.author_photo }}
                            />
                            <Text style={styles.caracteristiquesText}>{value.author_name}</Text>
                        </View>
                    )
                })}
                <View style={styles.commentairesParagraphContainer} >
                    <Text style={styles.commentairesParagraph}>
                        {comments?.length > 0 ? comments[0].content : null}
                    </Text>
                </View>
            </View>
            <View style={styles.UserCommentaireContainer}>
                {comments?.map((value, index, array) => {
                    return (
                        <View key={index} style={styles.UserImg}>
                            <Image
                                source={{ uri: value.author_photo }}
                            />
                            <Text style={styles.caracteristiquesText}>{value.author_name}</Text>
                        </View>
                    )
                })}
                <View style={styles.commentairesParagraphContainer} >
                    <Text style={styles.commentairesParagraph}>
                        {comments?.length > 0 ? comments[0].content : null}
                    </Text>
                </View>
            </View>
            <View style={styles.line}></View>
        </View>
    )
}

export default Commentaires

const styles = StyleSheet.create({
    commentairesContainer: {
        padding: 14
    },
    commentairesText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        top: 0,
    },
    UserImg: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        gap: 10,
    },
    UserName: {
        fontWeight: 'bold',
        fontSize: 16,
        color: 'black',
    },
    commentairesParagraphContainer: {
        marginTop: 10,
    },
    commentairesParagraph: {
        fontSize: 16,
        color: 'black',
        lineHeight: 20,
    },
    UserCommentaireContainer: {
        marginTop: 20,

    },
    line: {
        height: 1,
        backgroundColor: '#E5E5E5',
        marginVertical: 10,
        top: 150,
        width: '108%',
        position: 'absolute',
        left: 0,
    },
})

import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';


const Conditions = () => {
    const [selectedStartDate, setSelectedStartDate] = useState(null);
    const [selectedEndDate, setSelectedEndDate] = useState(null);


    const onDateChange = (date, type) => {
        if (type === 'END_DATE') {
            setSelectedEndDate(date);
        } else {
            setSelectedStartDate(date);
            setSelectedEndDate(null); // Reset end date if start date changes
        }
    };


    return (
        <View style={styles.conditionsContainer}>
            <Text style={styles.conditionsTitle}>Disponibilités de l'hébergement</Text>
            <View>
                <CalendarPicker
                    onDateChange={onDateChange}
                    selectedStartDate={selectedStartDate}
                    selectedEndDate={selectedEndDate}
                    startFromMonday={true}
                    allowRangeSelection={true}
                    selectedRangeStyle={{ backgroundColor: 'blue', opacity: 0.5 }}
                    weekdays={["Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"]}
                    months={[
                        "Janvier",
                        "Février",
                        "Mars",
                        "Avril",
                        "Mai",
                        "Juin",
                        "Juillet",
                        "Août",
                        "Septembre",
                        "Octobre",
                        "Novembre",
                        "Decembre",
                    ]}
                    previousTitle="Précédent"
                    nextTitle="Suivant"
                />
            </View>
            <View style={styles.line}></View>
        </View>
    )
}


export default Conditions

const styles = StyleSheet.create({
    conditionsContainer: {
        padding: 14,
        marginTop: 30,
    },
    conditionsTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        position: 'relative',
        top: -40,
    },
    line: {
        height: 1,
        backgroundColor: '#E5E5E5',
        marginVertical: 10,
    },
})
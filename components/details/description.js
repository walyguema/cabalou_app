import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Description = ({property}) => {
    
    return (
        <View style={styles.descriptionContainer}>
            <Text style={styles.descriptionTitle} >Description</Text>
            <Text style={styles.descriptionText} >
                {property?.description}
            </Text>
        </View>
    )
}

export default Description

const styles = StyleSheet.create({
    descriptionContainer: {
        padding: 14,
    },
    descriptionTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        position: 'relative',
        top: -50,
    },
    descriptionText: {
        fontSize: 16,
        color: 'black',
        position: 'relative',
        top: -40,
        textAlign: 'justify',
    }
})
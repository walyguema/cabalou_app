import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import React from 'react'

const Questions = () => {
    return (
        <View style={styles.questionsContainer}>
            <Text style={styles.questionsTitle}>Des questions? Contactez l'hébergeur</Text>
            <TextInput
                style={styles.questionsInput}
                placeholder="Tapez votre message ici..."
                multiline={true}
            />
            <View>
                <TouchableOpacity style={styles.questionsContactBtn} >
                    <Text style={styles.questionsContactText}>Envoyer le message</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.line}></View>
        </View>
    )
}

export default Questions

const styles = StyleSheet.create({
    questionsContainer: {
        padding: 14

    },
    questionsTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        top: -10,
    },
    questionsInput: {
        height: 150,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#D3D3D3',
        marginTop: 10,
        padding: 10,
        textAlignVertical: 'top',
    },
    questionsContactBtn: {
        display: 'flex',
        width: 200,
        padding: 8,
        marginTop: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#0050CF',
        alignItems: 'center',
    },
    questionsContactText: {
        color: '#0050CF',
        fontSize: 16,
        fontWeight: 'bold',
    },
    line: {
        height: 1,
        backgroundColor: '#E5E5E5', 
        marginVertical: 10,
        top: 10,
      },
})
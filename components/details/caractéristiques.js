import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import SvgUri from 'react-native-svg-uri';

const Caractéristiques = ({property}) => {

    return (
        <View style={styles.caracteristiquesContainer}>
            <Text style={styles.caracteristiquesTitle}>Caractéristiques</Text>
            <View style={styles.caracteristiquesAllIcons}>
                {property?.features.map((value, index, array) => {
                    return (
                        <View key={index} style={styles.caracteristiquesIconContainer}>
                            <SvgUri
                                width="30"
                                height="30"
                                source={{ uri: value.image }}
                            />
                            <Text style={styles.caracteristiquesText}>{value.name}</Text>
                        </View>
                    )
                })}
            </View>
            <View style={styles.line}></View>
        </View>
    );
}

export default Caractéristiques

const styles = StyleSheet.create({
    caracteristiquesIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 10,
        marginTop: 15,
    },
    caracteristiquesText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black',
    },
    caracteristiquesContainer: {
        padding: 14,
    },
    caracteristiquesTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        position: 'relative',
        top: -50,
    },
    caracteristiquesAllIcons: {
        position: 'relative',
        top: -35,
    },
    line: {
        height: 1,
        backgroundColor: '#E5E5E5',
        marginVertical: 10,
    },
})
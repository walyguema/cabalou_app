import { Image, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import SvgUri from 'react-native-svg-uri';
import { useRoute } from '@react-navigation/native';
import { getProperty } from '../../service/api';

const Conditions = ({property}) => {

    return (
        <View style={styles.conditionsContainer}>
            <Text style={styles.conditionsTitle}>Conditions</Text>
            <View style={styles.conditionsAllIcons}>
                {property?.rules.map((value, index, array) => (
                    <View key={index} style={styles.conditionsIconContainer}>
                        <SvgUri
                            width="30"
                            height="30"
                            source={{ uri: value.icon }}
                        />
                        <Text style={styles.conditionsText}>{value.name}</Text>
                    </View>
                ))}
            </View>
            <View style={styles.line}></View>
        </View>
    )
}

export default Conditions

const styles = StyleSheet.create({
    conditionsIconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 10,
        marginTop: 15,
    },
    conditionsText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black',
    },
    conditionsContainer: {
        padding: 14,
        marginTop: 30,
    },
    conditionsTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',
        position: 'relative',
        top: -40,
    },
    conditionsAllIcons: {
        position: 'relative',
        top: -35,
    },
    line: {
        height: 1,
        backgroundColor: '#E5E5E5',
        marginVertical: 10,
    },
})
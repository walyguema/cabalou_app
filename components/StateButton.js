import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';


/**
 * 
 * @param {{states : [{value : any,text : string,icon : React.JSX.Element}],onChange : ({value : any}) => void}} param0 
 * @returns 
 */
const StateButton = ({states = [],onChange}) => {
    const [index,setIndex] = useState(0);
    const onClick = () =>{
        setIndex(val => {
            
            if(val < states.length - 1){
                return val + 1;
            }else{
                return 0;
            }
        });
        if(onChange){
            onChange({value : states[index].value})
        }
    }
    return (
        <TouchableOpacity style={style.container} onPress={onClick}>
            <View style={style.icon}>
                <View>{states.length >= 1 ? states[index]?.icon ?? "null" : null}</View>
            </View>
            <View style={style.containerText}>
                <Text style={style.text}>{states.length >= 1 ? states[index]?.text ?? "null" : null}</Text>
            </View>
        </TouchableOpacity>
    );
}
const style = StyleSheet.create({
    container : {
        flexDirection : "row",
        height : 45,
        borderWidth : 2,
        borderRadius : 4,
        backgroundColor : "#fff",
        borderColor : "#aaa"
    },
    icon : {
        flexBasis : 45,
        justifyContent : "center",
        alignItems : "center"
    },
    containerText : {
        flex : 1,
        justifyContent : "center",
        alignItems : "center"
    },
    text : {
        color : "#000"
    }
})
export default StateButton;
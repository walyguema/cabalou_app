import { ScrollView, StyleSheet, Text } from "react-native";
import VerticalView from "../VerticalView";
import Lorem from "../Lorem";
import InputFontIcon from "../inputs/InputFontIcon";
import InputIconSelect from "../inputs/InputIconSelect";

const Informations = () => {
    return (
        <VerticalView style={style.container}>
            <InputFontIcon placeholder="Titre de l'hébergement" icon="home-city-outline"/>
            <InputIconSelect/>
            {/* <InputFontIcon placeholder="Titre de l'hébergement" icon="home-city-outline"/> */}
        </VerticalView>
    );
}
const style = StyleSheet.create({
    container : {
        paddingHorizontal : 10,
    }
})
export default Informations;
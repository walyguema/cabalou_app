import React from 'react';
import {Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {ASSETS} from '../../utils/assets';
import {radius} from '../../utils/functions';
const SwiperScreen = ({title, desc, img, onClick}) => {
  return (
    <View style={style.container}>
      <Image style={style.image} source={img} />
      <View style={style.containerText}>
        <Text style={style.title}>{title}</Text>
        <Text style={style.desc}>{desc}</Text>
        <TouchableOpacity
          onPress={() => {
            onClick();
          }}
          style={style.button}>
          <Text style={style.textButton}>Suivant</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const style = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  image: {
    width: '100%',
    height: '100%',
    objectFit: 'cover',
  },
  containerText: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 100,
    paddingHorizontal: 20,
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.2)',
    ...radius(80, 80, 0, 0),
  },
  title: {
    fontSize: 50,
    color: '#fff',
    fontWeight: 'bold',
  },
  desc: {
    fontSize: 20,
    color: '#fff',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: 'royalblue',
    width: '100%',
    marginTop: 20,
    borderRadius: 10,
  },
  textButton: {
    color: '#fff',
    fontSize: 17,
    fontWeight: 'bold',
  },
});
export default SwiperScreen;

import {StyleSheet, View} from 'react-native';

/**
 *
 * @param {{style : StyleSheet.create,children : React.JSX.Element}} param0
 * @returns
 */
const ContainerShadow = ({style, children}) => {
  return (
    <View style={{...styleComponent.contenaire, ...style}}>{children}</View>
  );
};
const styleComponent = StyleSheet.create({
  contenaire: {
    width: '100%',
    padding: 10,
    marginVertical: 4,
    borderRadius: 5,
    backgroundColor: '#fff',
    shadowColor: '#000',
    borderWidth: 1,
    borderColor: '#eee',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.18,
    shadowRadius: 4.59,
    elevation: 5,
  },
});
export default ContainerShadow;

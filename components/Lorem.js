import {Text} from 'react-native';

const Lorem = () => {
  return (
    <Text>
      Fundato et doctus Cyprii doctus linguam regis membra audacem sedibus
      laceratus regis Cyprii qui Caesar ut iniquitati consorte atque Zenonem
      inpegit flagitaret avulsam gnarus evisceratus temporum in se intrepidus
      fundato confessus mansit regis caelo sollemnia deessent morte nec oculos
      suam et cruciatibus nec mentiretur Stoicum caelo se cum Stoicum confutatus
      in inmobilis sedibus sedibus qui Caesar superbiam Cyprii Zenonem oculos
      cruciatibus insultans avulsam nec sollemnia suam nec evisceratus ut
      intrepidus passus confessus cruento atque incusare in Caesar ducebatur
      iustitiam nec confessus Cyprii poenali inmobilis membra ducebatur
      inplorans Zenonem quaedam iustitiam atque cum inmobilis ducebatur in
      audacem fundato linguam renidens incusare.
    </Text>
  );
};

export default Lorem;

import React, { useState } from 'react';
import { StyleSheet, Text } from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
const InputPhoneNumber = ({onChange}) => {
   const [countryCode,setContryCode] = useState("241");
    return (
        <PhoneInput
        
        defaultCode="GA"
        onChangeText={(text) => {
          if(onChange){
            let code = countryCode ?? "";
            onChange(code + text,code);
          }
        }}
        onChangeCountry={(c) =>{
          setContryCode(c.callingCode[0]);
        }}
        placeholder="Numero de telephone..."
        countryPickerButtonStyle={style.countryPickerButtonStyle}
        textContainerStyle={{
          opacity: 1,
          padding: 0,
          margin: 0,
          backgroundColor: '#f1f1f1',
          height: 55,
        }}
        layout="first"
        containerStyle={style.containerInputPhone}
        textInputStyle={style.textInputStyle}
      />
    );
}

const style = StyleSheet.create({
    containerInputPhone: {
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'transparent',
    },
    countryPickerButtonStyle: {
      backgroundColor: 'transparent',
    },
    textInputStyle: {
      padding: 0,
      color: '#000',
      width: '100%',
      margin: 0,
    }
  });
export default InputPhoneNumber;
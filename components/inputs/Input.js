import React from 'react';
import { Image, StyleSheet, Text, TextInput, View } from "react-native";
/**
 * 
 * @param {{placeholder : string,icon : NodeRequire}} param
 * @returns 
 */
const Input = ({placeholder,icon}) => {
    return (
        <>
        <View style={{...style.container}}>
            <TextInput placeholder={placeholder} style={{...style.input}}/>
            <View style={{...style.icon}}>
                <Image source={icon} style={style.iconImg}/>
            </View>
        </View>
        </>
    );
}
const style = StyleSheet.create({
    container : {
        flexDirection : "row",
        borderWidth : 2,
        borderRadius : 8,
        borderColor : "#D3D8E6"
    },
    icon : {
        width : 50,
        justifyContent : "center",
        alignItems : "center",
    },
    iconImg : {
        width : 18,
        height : 18,
        objectFit : "contain",
        tintColor : "#D3D8E6"
    },
    input : {
        flex : 1,
        paddingHorizontal : 10,
        height : 43
    }
})
export default Input;
import { StyleSheet, TextInput, View } from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const InputFontIcon = ({icon = "account",placeholder = ""}) => {
    return (
        <>
            <View style={style.container}>
                <View style={style.icon}>
                    <Icon name={icon} size={23} color="#fff"/>
                </View>
                <TextInput placeholder={placeholder} style={style.input}/>
            </View>
        </>
    );
}
const style = StyleSheet.create({
    container : {
        flexDirection : "row",
        height : 50,
        overflow : "hidden",
        borderRadius : 4,
        marginVertical : 5,
    },
    icon : {
        width : 50,
        backgroundColor : "#1E1777",
        justifyContent : "center",
        alignItems : 'center'
    },
    input : {
        backgroundColor : '#F3F3F3',
        flex : 1,
        paddingHorizontal : 10,
        color : "#000"
    }
})
export default InputFontIcon;
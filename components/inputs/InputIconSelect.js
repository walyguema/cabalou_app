import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {addShadow} from '../../utils/functions';
import {useRef} from 'react';

const InputIconSelect = () => {
  const options = [
    {
      icon: 'home',
      label: 'Go to home',
    },
    {
      icon: 'chess-pawn',
      label: 'chess pawn',
    },
    {
      icon: 'chart-timeline',
      label: 'chart timeline',
    },
    {
      icon: 'cash-lock',
      label: 'cash locked',
    },
  ];
  return (
    <>
      <TouchableOpacity style={style.conatainer}>
        <View style={style.input}>
          <Icon name={options[1].icon} size={23} color="#555" />
          <Text style={style.textSlected}>Here, you add youy text...</Text>
        </View>
        <View style={style.iconSlect}>
          <Icon name="chevron-down" size={29} color="#fff" />
        </View>
        
      <View style={style.options}>
          {options.map((option, i, _) => (
            <View style={{flexDirection : "row",height : 40}} key={i}>
              <Icon name={option.icon} size={22} />
              <Text>{option.label}</Text>
            </View>
          ))}
        </View>
      </TouchableOpacity>
    </>
  );
};
const style = StyleSheet.create({
  conatainer: {
    flexDirection: 'row',
    overflow : "hidden",
    height: 50,
    borderRadius: 4,
    marginVertical: 5,
    position: 'relative',
    zIndex: 100,
  },
  input: {
    flexDirection: 'row',
    backgroundColor: '#F3F3F3',
    flex: 1,
    alignItems: 'center',
    gap: 10,
    paddingHorizontal: 10,
  },
  iconSlect: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1E1777',
  },
  textSlected: {
    color: '#000',
  },
  options: {
    position: 'absolute',
    backgroundColor: '#fff',
    width: '100%',
    ...addShadow(),
    // top : 0,
    // bottom: "-20%",
    // transform: [{translateY: 10}],
    zIndex: 1000,
    // bottom : 0
    // top : 0
  },
});
export default InputIconSelect;

import React from 'react';
import { Image, StyleSheet, Text, TextInput, View } from 'react-native';

/**
 * 
 * @param {{icon : NodeRequire,placeholder : string,value : string,type : keyboardType,editable : boolean,onChange : (text : string) => void}} param0 
 * @returns 
 */
const InputIcon = ({icon,placeholder,value,type,editable = true,onChange}) => {
    return (
        <View style={style.container}>
            <View style={style.icon}>
                {icon}
            </View>
            <TextInput editable={editable} value={value} keyboardType={type ?? 'email-address'} style={style.input} placeholder={placeholder ?? 'Add placeholder'} onChangeText={text =>{
                if(onChange){
                    onChange(text);
                }
            }}/>
        </View>
    );
}
const style = StyleSheet.create({
    container : {
        width : "100%",
        height : 55,
        flexDirection : "row"
    },
    icon : {
        width : 45,
        justifyContent : "center",
        alignItems : "center"
    },
    input : {
        backgroundColor : "#eee",
        flex : 1,
        paddingHorizontal : 10
    }
})
export default InputIcon;
import React, {useState} from 'react';
import {
  Button,
  FlatList,
  Image,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ASSETS} from '../../utils/assets';
import InputIcon from './InputIcon';
import CountryFlag from 'react-native-country-flag';
import EmptyData from '../requestRender/EmptyData';
import Loader from '../Loader';
/**
 *
 * @param {{items? : [any],onSelectItem? : (item : any) => void}} param0
 * @returns
 */
const InputSelect = ({items = [], onSelectItem}) => {
  const [showModal, setShowModal] = useState(false);
  const [pays, setPays] = useState({});
  const [displayCountry, setDisplayCountry] = useState(20);
  const [allPays,setAllPays] = useState([...items]);

  const onSelectPays = pays => {
    if (onSelectItem) {
      onSelectItem(pays);
    }
    setPays(pays);
    setShowModal(!showModal);
  };

  const onSearch = (text) =>{
    let tab = items.filter((it,i,_) => it.name.toLowerCase().match(text.toLowerCase()));
    setAllPays(tab);
  }

  return (
    <>
      <TouchableOpacity
        style={style.conatainerButton}
        onPress={() => {
          setAllPays(items);
          setShowModal(!showModal);
        }}>
        <View>
          {pays?.iso_code ? (
            <CountryFlag isoCode={pays.iso_code} size={20} />
          ) : (
            <Image style={style.iconInput} source={ASSETS.GLOBAL.galerie} />
          )}
        </View>
        <Text style={style.textSelect}>
          {pays?.name ? pays.name : 'Depuis quel pays souhaitez-vous payer ?'}
        </Text>
        <View>
          <Image style={style.iconArrow} source={ASSETS.GLOBAL.arrow_bottom} />
        </View>
      </TouchableOpacity>
      {/* modal */}
      <Modal animationType="slide" visible={showModal}>
        <View style={style.conatainerModal}>
          <View>
            <InputIcon
              icon={
                <Image
                  style={{width: 20, height: 20}}
                  source={ASSETS.SEARSH.recherche}
                />
              }
              placeholder="Recherche..."
              onChange={(text) => {onSearch(text)}}
            />
            <FlatList
              data={allPays.filter((_, i, __) => i <= displayCountry)}
              onEndReachedThreshold={0.4}
              renderItem={({item, i, sepators}) => (
                <TouchableOpacity
                    style={style.itemPays}
                    key={i}
                    onPress={() => {
                      onSelectPays(item);
                    }}>
                    <View>
                      <CountryFlag isoCode={item.iso_code} size={20}/>
                    </View>
                    <Text style={{color: '#000'}}>{item.name}</Text>
                  </TouchableOpacity>
              )}
              ListFooterComponent={
                !allPays.length ? <EmptyData/> : allPays.length <= 14 ? null : <View style={{height : 100,width : "100%"}}><Loader/></View>
              }
              onEndReached={() => {
                setDisplayCountry(displayCountry + 5);
              }}
            />
          
          </View>
        </View>
      </Modal>
      {/* fin modal */}
    </>
  );
};
const style = StyleSheet.create({
  conatainerButton: {
    width: '100%',
    height: 50,
    borderWidth: 2,
    borderColor: '#aaa',
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
    gap: 7,
    backgroundColor: '#fff',
  },
  iconInput: {
    width: 25,
    height: 25,
    tintColor: '#888',
  },
  iconArrow: {
    width: 12,
    height: 12,
    tintColor: '#888',
  },
  textSelect: {
    flex: 1,
    color: '#000',
  },
  conatainerModal: {
    justifyContent: 'space-between',
    flex: 1,
    padding: 10,
  },
  itemPays: {
    width: '100%',
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
    gap: 10,
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  conatainerItem: {
    gap: 10,
    paddingVertical: 30,
  },
});
export default InputSelect;

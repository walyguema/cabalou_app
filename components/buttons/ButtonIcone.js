import React from 'react';
import { Text, View,TouchableOpacity, StyleSheet, Image } from 'react-native';
import { ASSETS } from '../../utils/assets';
/**
 * 
 * @param {{text : string,icon : NodeRequire,onPress : () => void}} param0 
 * @returns 
 */
const ButtonIcone = ({text,icon,onPress}) => {
    const onClick = () =>{
        if(onPress){
            onPress();
        }
    }
    return (
        <>
            <TouchableOpacity style={style.container} onPress={onClick}>
                <View style={style.icon}>
                    <Image style={style.iconImg} source={icon ?? ASSETS.GLOBAL.galerie}/>
                </View>
                <Text style={style.text}>{text ?? "Votre texte ici..."}</Text>
            </TouchableOpacity>
        </>
    );
}
const style = StyleSheet.create({
    container : {
        paddingHorizontal : 20,
        paddingVertical : 20,
        marginVertical : 5,
        flexDirection : "row",
        alignItems : "center",
        gap : 10,
        backgroundColor : "rgba(0,0,0,0.01)",
        borderRadius : 10
    },
    icon : {
        backgroundColor : "royalblue",
        padding : 10,
        borderRadius : 10
    },
    iconImg : {
        width : 20,
        height : 20,
        objectFit : "contain",
        tintColor : "#fff"
    },
    text : {
        fontSize : 18,
        color : "#000"
    }
})
export default ButtonIcone;
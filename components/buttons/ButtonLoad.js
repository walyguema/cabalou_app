import React from 'react';
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity } from "react-native";

/**
 * 
 * @param {{active : boolean,texte : string,onClick : () => void}} param0 
 * @returns 
 */
const ButtonLoad = ({active = false,texte,onClick}) => {
    const clickBtn = () =>{
        if(onClick && !active){
            onClick();
        }
    }
    return (
        <TouchableOpacity style={{...mainStyle.container}} onPress={clickBtn}>
        <ActivityIndicator size="small" color="royalblue" style={{...mainStyle.loader,display : active ? "flex" : "none"}}/>
            <Text style={mainStyle.text}>{texte}</Text>
        </TouchableOpacity>
    );
}
const mainStyle = StyleSheet.create({
    container : {
        width : "100%",
        height : 50,
        backgroundColor : "#0050CF",
        justifyContent : "center",
        alignItems : "center",
        borderRadius : 2,
        position : "relative"
    },
    text : {
        color : "#fff"
    },
    loader : {
        backgroundColor : "rgba(0,0,0,0.6)",
        width : "100%",
        height : "100%",
        position : "absolute",
        zIndex : 2
    }
})
export default ButtonLoad;
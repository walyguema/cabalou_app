import React, {useState} from 'react';
import {Text} from 'react-native';
import Login from './Login/Login';
import LoginPassword from './LoginPassword/LoginPassword';
import Register from './Register/Register';
const AuthUser = () => {
  const [data, setData] = useState({
    status: null,
    form: 'login',
    type: null,
    value: null,
    data: null,
    code : ""
  });
  return (
    <>
      <Login
        form={data.form}
        onSubmit={resp => {
          setData(resp);
        }}
      />
      <LoginPassword form={data.form} email={data.data?.email}/>
      <Register code={data.code} value={data.value} useMail={data.type == "EMAIL" ? true : false} form={data.form} />
    </>
  );
};

export default AuthUser;

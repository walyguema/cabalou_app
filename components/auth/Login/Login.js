import React, { useState } from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
import ButtonLoad from '../../buttons/ButtonLoad';
import {GoogleSigninButton} from '@react-native-google-signin/google-signin';
import StateButton from '../../StateButton';
import InputIcon from '../../inputs/InputIcon';
import { ASSETS } from '../../../utils/assets';
import InputPhoneNumber from '../../inputs/InputPhoneNumber';
import ModalOtp from '../Modal/ModalOtp';
import { checkAccount, sendOtp, sendUserOtp } from '../../../service/authentification';
import { useDispatch } from 'react-redux';
import { loginUser } from '../../../redux/actions/userSlice';
/**
 * 
 * @param {{form : string,onSubmit : function({status : boolean,form : string,type : "PHONE" | "EMAIL",value : string,data : any})}} param0 
 * @returns 
 */
const Login = ({form,onSubmit}) => {
    const [visible,setVisible] = useState(false);
    const [useEmail,setUseEmail] = useState(false);
    const [value,setValue] = useState({value : null,code : null});
    const [loaderButton,setLoaderButton] = useState(false);
    const dispatch = useDispatch();

    const onChangeValue = (value) =>{
      setValue(value);
    }
    const loginByOtp = () =>{
      if(useEmail){
        sendUserOtp({headers : {type : "EMAIL"},data : {email : value.value}}).then(res =>{
          console.log(res.status);
          if(res.status == 200){
            setVisible(true);
          }
        })
      }else{
        console.log("OTP PAR NUMERO DE TELEPHONE");
      }
    }
    const login = () =>{
      setLoaderButton(true);
      checkAccount({query : {type : useEmail ? "EMAIL" : "PHONE",value : value.value}}).then(res =>{
        console.log(res);
        if(res.status == 200){
          setLoaderButton(false);
          if(res.data.user.password_is_set){
            onSubmit({status : false,form : "password",type : useEmail ? "EMAIL" : "PHONE",value : value.value,data : res.data.user});
          }else{
            loginByOtp();
          }
        }else{
          setLoaderButton(false);
          console.log(res);
          console.log("Pas de compte");
          loginByOtp();
        }
      }).catch(err =>{
        console.log(err);
        // console.log(res);
        setLoaderButton(false);
      })
    }
  return (
    <>
    <ModalOtp value={value} visible={visible} type={useEmail ? "EMAIL" : "PHONE"} onSucces={(resp) => {
      if(resp.status){
        dispatch(loginUser(resp.data));
      }else{
        if(onSubmit){
          onSubmit(resp);
        }
      }
        setVisible(false);
    }}/>
    <View style={{...style.container,display : form == "login" ? "flex" : "none"}}>
    {/* <Text style={{fontSize : 20,color : "#000"}}>{value.value}</Text> */}
      <Text style={style.title}>
        Connectez-vous ou Inscrivez-vous pour réserver
      </Text>
      {useEmail ? <InputIcon onChange={(text) =>{
        onChangeValue({value : text,code : ""});
        }} icon={<Image style={style.iconImg} source={ASSETS.GLOBAL.mail}/>} placeholder='Votre adresse e-mail...'/> : <InputPhoneNumber onChange={(val,code) =>{
        console.log(val,code)
        onChangeValue({value : val,code : code});
        }}/>}
      <ButtonLoad
        texte="Continuer la demande de reservation"
        active={loaderButton}
        onClick={login}
      />
      <View style={style.containerOr}>
        <View style={style.borderOr}></View>
        <Text style={style.textOr}>Ou</Text>
      </View>
      <GoogleSigninButton
        style={{width: '100%'}}
        size={GoogleSigninButton.Size.Wide}
      />
      <StateButton
        states={[
          {
            icon: <Image style={style.iconStateButton} source={ASSETS.GLOBAL.mail}/>,
            value: true,
            text: 'Continuer avec un e-mail',
          },
          {
            icon: <Image style={style.iconStateButton} source={ASSETS.GLOBAL.phone}/>,
            value: false,
            text: 'Continuer avec un numero de telephone',
          },
        ]}
        onChange={val => {
        //   console.log('On passe au : ' + val.value);
        setUseEmail(val.value);
        }}
      />
    </View>
    </>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    marginVertical: 20,
    paddingVertical: 10,
    gap: 20,
  },
  title: {
    fontSize: 22,
    color: '#000',
    textAlign: 'center',
    marginBottom: 20,
  },
  containerOr: {
    position: 'relative',
    width: '100%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  borderOr: {
    position: 'absolute',
    width: '100%',
    height: 2,
    backgroundColor: '#888',
  },
  textOr: {
    backgroundColor: '#fafafa',
    paddingHorizontal: 20,
    paddingVertical: 3,
    fontSize: 18,
    color: '#555',
  },
  iconStateButton : {
  width : 25,
  height : 25,
  objectFit : "cover",
  tintColor : "#888"
  },
  iconImg : {
        width : 25,
        height : 25
    },
});
export default Login;

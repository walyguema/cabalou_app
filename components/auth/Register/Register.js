import React, {useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import InputIcon from '../../inputs/InputIcon';
import {ASSETS} from '../../../utils/assets';
import InputPhoneNumber from '../../inputs/InputPhoneNumber';
import ButtonLoad from '../../buttons/ButtonLoad';
import CheckBox from '@react-native-community/checkbox';
import { register } from '../../../service/authentification';
import { useDispatch } from 'react-redux';
import { loginUser } from '../../../redux/actions/userSlice';

const Register = ({value,code,form,useMail = false}) => {
  const [check, setCheck] = useState(false);
  const [name,setName] = useState("");
  const [lastName,setLastName] = useState("");
  const [dynamicInput,setDynamicInput] = useState("");
  const [phoneCode,setPhoneCode] = useState(code);
  const [loading,setLoading] = useState(false);
  const dispatch = useDispatch();
  
  const datas = {
    type : "client",
    last_name : name,
    first_name : lastName,
    phone_code : phoneCode ?? code,
    password : "no",
    password_confirmation : "no",
    company_name : "",
    company_number : "",
    email : useMail ? value : dynamicInput,
    next_url : "",
    phone : useMail ? dynamicInput : value
  };
  const registerUser = () =>{
    register(datas).then(res =>{
      if(res.status == 201){
        setLoading(false);
        dispatch(loginUser(res.data.user));
      }else{
        console.log('====================================');
        console.log(res);
        console.log('====================================');
      }
    }).catch(err =>{
      setLoading(false);
    })
  }
  return (
    <View style={{...style.container,display : form == "register" ? "flex" : "none"}}>
      <Text style={style.title}>
        Completez les informations de votre compte
      </Text>
      <View style={style.containerInput}>
        <InputIcon
          placeholder="Votre nom..."
          icon={<Image style={style.icon} source={ASSETS.ICONSTAB.profil} />}
          onChange={(text =>{
            setName(text);
          })}
        />
        <InputIcon
          placeholder="Votre prenom..."
          icon={<Image style={style.icon} source={ASSETS.ICONSTAB.profil} />}
          onChange={(text) =>{
            setLastName(text);
          }}
        />
        {!useMail ? (
          <InputIcon
            placeholder="Votre e-mail..."
            icon={<Image style={style.icon} source={ASSETS.GLOBAL.mail} />}
            onChange={(text) =>{
              setDynamicInput(text);
            }}
          />
        ) : (
          <InputPhoneNumber onChange={(num,cde) =>{
            setDynamicInput(num);
            setPhoneCode(cde)
          }}/>
        )}
        <View style={{...style.containerCheck,opacity : form == "register" ? 1 : 0}}>
          <CheckBox
            value={check}
            onValueChange={val => {
              setCheck(val);
            }}
          />
          <Text>
            J'accepte les{' '}
            <Text style={{color: 'royalblue'}}>conditions d'utilisation</Text>
          </Text>
        </View>
      </View>
      <ButtonLoad onClick={registerUser} active={loading} texte="Cree mon compte" />
    </View>
  );
};
const style = StyleSheet.create({
  containerInput: {
    gap: 15,
    // backgroundColor : "orange",
    marginBottom: 20,
  },
  container: {
    gap: 10,
    marginVertical: 30,
  },
  icon: {
    width: 25,
    height: 25,
  },
  title: {
    fontSize: 23,
    textAlign: 'center',
    color: '#222',
    fontWeight: 'bold',
    marginBottom: 20,
  },
  containerCheck: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
export default Register;

import React, {useRef, useState} from 'react';
import {Button, Keyboard, Modal, Pressable, StyleSheet, Text, TextInput, View} from 'react-native';
import Loader from '../../Loader';
import { verifyOtpSendUser } from '../../../service/api';

/**
 * 
 * @param {{visible : boolean,value : {value : string,code : string},type : "PHONE" | "EMAIL",onSucces : function({status : boolean,form : string,type : "PHONE" | "EMAIL",value : string,data : any,countryCode : string})}} param0 
 * @returns 
 */
const ModalOtp = ({visible = true,value,type = "",onSucces}) => {

  const [otpValue,setOtpValue] = useState("");
  const [loader,setLoader] = useState(false);
  const inputRef = useRef();

  const verifyOtp = (code) =>{

    verifyOtpSendUser({headers : {type : type},data : {email : value.value,code : code}}).then(res =>{
      if(res.status == 200){
        setLoader(false);
        setOtpValue("");

          if(res.data.user){
            if(onSucces){
              onSucces({status : true,data : res.data.user});
            }
          }else{
            if(onSucces){
              onSucces({status : false,form : "register",type : type,value : value.value,data : null,code : value.code});
            }
          }
          
      }else{
        setLoader(false);
        setOtpValue("");
        console.log("OTP ECHEC");
      }
    }).catch(err =>{
      console.log("IL Y A EU UNE ERREUR");
    });
  }
  const activeKeyBoard = () =>{
    inputRef.current.focus();
  }

  const changeText = (text) =>{
    setOtpValue(text);
    if(text.length >= 6){
      verifyOtp(text);
      setLoader(true);
      Keyboard.dismiss();
    }else{
      setLoader(false);
      console.log("pas 6");
    }
  }
  return (
    <Modal animationType="slide" visible={visible}>
      <View style={style.container}>
        <Text style={style.title}>Confirmez votre {type == "EMAIL" ? "e-mail" : "numero de telephone"}</Text>
        <Text style={style.desc}>
          Saisissez le code que vous avez recu par {type == "EMAIL" ? "e-mail" : "numero de telephone"}.
        </Text>
        <View style={style.containerOtp}>
            <Pressable onPress={activeKeyBoard} style={{...style.inputsOtp,borderColor : otpValue[0] ? "royalblue" : "#aaa"}}><Text style={style.textOtp}>{otpValue[0] ?? "-"}</Text></Pressable>
            <Pressable onPress={activeKeyBoard} style={{...style.inputsOtp,borderColor : otpValue[1] ? "royalblue" : "#aaa"}}><Text style={style.textOtp}>{otpValue[1] ?? "-"}</Text></Pressable>
            <Pressable onPress={activeKeyBoard} style={{...style.inputsOtp,borderColor : otpValue[2] ? "royalblue" : "#aaa"}}><Text style={style.textOtp}>{otpValue[2] ?? "-"}</Text></Pressable>
            <Pressable onPress={activeKeyBoard} style={{...style.inputsOtp,borderColor : otpValue[3] ? "royalblue" : "#aaa"}}><Text style={style.textOtp}>{otpValue[3] ?? "-"}</Text></Pressable>
            <Pressable onPress={activeKeyBoard} style={{...style.inputsOtp,borderColor : otpValue[4] ? "royalblue" : "#aaa"}}><Text style={style.textOtp}>{otpValue[4] ?? "-"}</Text></Pressable>
            <Pressable onPress={activeKeyBoard} style={{...style.inputsOtp,borderColor : otpValue[5] ? "royalblue" : "#aaa"}}><Text style={style.textOtp}>{otpValue[5] ?? "-"}</Text></Pressable>
            <TextInput style={style.disableInput} maxLength={6} ref={inputRef} value={otpValue} keyboardType="numeric" onChange={(val) =>{changeText(val.nativeEvent.text)}}/>
        </View>
        <View style={style.containerLoader}>
        <Loader active={loader}/>
        </View>
      </View>
    </Modal>
  );
};
const style = StyleSheet.create({
  container: {
    paddingVertical: 30,
    paddingHorizontal : 10,
    alignItems : "center"
  },
  title : {
    color : "#000",
    fontSize : 20,
    textAlign : "center",
    marginVertical : 20
  },
  desc : {
    textAlign : "center",
    color : "#555",
    fontSize : 16,
    marginBottom : 20
  },
  containerOtp : {
    position : "relative",
    flexDirection : "row",
    width : "100%",
    maxWidth : 400,
    height : 45,
    gap : 10,
    paddingHorizontal : 10,
    marginTop : 30
  },
  inputsOtp : {
    borderWidth : 2,
    borderColor : "#aaa",
    flex : 1,
    justifyContent : "center",
    alignItems : "center",
    borderRadius : 6
  },
  disableInput : {
    position : "absolute",
    width : 0,
    height : 0,
    left : 0,
    top : 0,
    opacity : 0
  },
  textOtp : {
    color : "#000",
    fontSize : 20,
    fontWeight : "bold"
  },
  containerLoader : {
    width : "100%",
    marginVertical : 30
  }
});
export default ModalOtp;

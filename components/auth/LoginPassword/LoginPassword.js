import React, { useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import InputIcon from '../../inputs/InputIcon';
import ButtonLoad from '../../buttons/ButtonLoad';
import { ASSETS } from '../../../utils/assets';
import { authorize, login } from '../../../service/authentification';
import { useDispatch } from 'react-redux';
import { loginUser } from '../../../redux/actions/userSlice';
import { setLogin } from '../../../redux/actions/authSlicer';

const LoginPassword = ({form,email}) => {
    const [password,setPassword] = useState("");
    const [loading,setLoading] = useState(false);
    const [error,setError] = useState(null);
    const dispatch = useDispatch();

    const loginWithPassword = () =>{

        setError(null);
        setLoading(true);
        
        login(password,email).then(res => {
            setLoading(false);
            if(res.status == 200){
                authorize(res.data.user.resfresh_token).then(auth =>{
                    dispatch(setLogin(auth.data.access_token));
                    dispatch(loginUser(res.data.user));
                })
            }else{
                setError("Mauvais mot de passe.");
            }
        }).catch(err =>{
            setLoading(false);
            setError("Mauvais mot de passe.");
        })
    }
    return (
        <View style={{...style.container,display : form == "password" ? "flex" : "none"}}>
            <Text style={style.title}>Connectez-vous par mot de passe</Text>
            <InputIcon icon={<Image style={style.icon} source={ASSETS.GLOBAL.mail}/>} editable={false} value={email}/>
            <InputIcon icon={<Image style={style.icon} source={ASSETS.GLOBAL.key}/>} placeholder='Votre mot de passe...' onChange={(text) =>{
                setPassword(text);
            }}/>
            {error ? <View style={style.error}>
                <Text style={style.textError}>{error}</Text>
            </View> : null}
            <ButtonLoad onClick={loginWithPassword} active={loading} texte='Connexion'/>
        </View>
    );
}
const style = StyleSheet.create({
    container : {
        gap : 20
    },
    title : {
        fontSize : 22,
        color : "#000",
        textAlign : "center",
        marginVertical : 20
    },
    icon : {
        width : 20,
        height : 20,
        tintColor : "#777"
    },
    error : {
        paddingVertical : 14,
        paddingHorizontal : 10,
        backgroundColor : "rgba(255,0,0,0.6)",
        borderRadius : 4
    },
    textError : {
        color : "#fff"
    }
})
export default LoginPassword;
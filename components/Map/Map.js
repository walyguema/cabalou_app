// import Ma from "react-native-maps";
import MapView from 'react-native-maps';
const Map = ({longitude = 9.4544,latitude = 0.3901}) => {
  return (
    <>
      <MapView
      style={{height : 300}}
        initialRegion={{
          latitude: latitude ?? 0.3901,
          longitude: longitude ?? 9.4544,
          latitudeDelta: 0.003,
          longitudeDelta: 0.003,
        }}
        
      />
    </>
  );
};

export default Map;

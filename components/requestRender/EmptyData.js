import React from 'react';
import { Image, StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { screenHeight } from '../../utils/variables';
import { ASSETS } from '../../utils/assets';
// do-not-disturb
const EmptyData = () => {
    return (
        <View style={style.container}>
           {/* <Image style={style.iconEmpty} source={ASSETS.GLOBAL.empty}/> */}
           <Icon name="do-not-disturb" color="#aaa" size={60}/>
           <Text style={style.text}>Aucune donnees</Text>
        </View>
    );
}
const style = StyleSheet.create({
    container : {
        height : screenHeight - 200,
        justifyContent : "center",
        alignItems : "center",
        gap : 20
    },
    text : {
        color : "#aaa",
        fontSize : 18,
        textAlign : "center"
    }
})
export default EmptyData;
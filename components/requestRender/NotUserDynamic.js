import { StyleSheet, Text, View } from "react-native";
import Icon from 'react-native-vector-icons/AntDesign';
import { screenHeight } from "../../utils/variables";
import ButtonLoad from "../buttons/ButtonLoad";

const NotUserDynamic = () => {
    return (
        <>
            <View style={style.container}>
                <Icon name="deleteuser" size={60}/>
                <Text style={style.text}>Connectez-vous a votre compte pour voir vos reservations.</Text>
                {/* <ButtonLoad active={true} texte="Connexion"/> */}
            </View>
        </>
    );
}
const style = StyleSheet.create({
    container : {
        height : screenHeight - 270,
        justifyContent : "center",
        alignItems : "center",
        gap : 20
    },
    text : {
        fontSize : 20,
        color : "#555",
        textAlign : "center",
        paddingHorizontal : 20
    }
})
export default NotUserDynamic;
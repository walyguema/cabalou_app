import React, { useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ASSETS } from '../../utils/assets';
import AuthUser from '../auth';
import ModalComponent from '../Modal/ModalComponent';
const NotUser = () => {
    const [visible,setVisible] = useState(false);
    return (
        <>
            <ModalComponent visible={visible} onClose={() => {setVisible(false)}}>
            <View style={style.containerModal}>
            <AuthUser/>
            </View>
            </ModalComponent>
            <View style={style.container}>
            <Image style={style.icon} source={ASSETS.ICONSTAB.profil}/>
            <TouchableOpacity style={style.button} onPress={() =>{
                setVisible(true);
            }}>
            <Text style={style.text}>Ajouter un compte</Text>
            </TouchableOpacity>
        </View>
        </>
    );
}
const style = StyleSheet.create({
    container : {
        height : 200,
        justifyContent : "center",
        alignItems : "center"
         
    },
    text : {
        color : "#fff",
        fontSize : 14,
        fontWeight : "bold"
    },
    icon : {
        tintColor : "#888"
    },
    button : {
        backgroundColor : "royalblue",
        borderRadius : 99,
        paddingVertical : 10,
        paddingHorizontal : 20
    },
    containerModal : {
        paddingVertical : 60,
        paddingHorizontal : 10
    }
})
export default NotUser;
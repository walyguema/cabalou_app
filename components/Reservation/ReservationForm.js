import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Rows from '../Rows';
import ButtonLoad from '../buttons/ButtonLoad';
import ContainerShadow from '../ContainerShadow';
const ReservationForm = () => {
  return (
    <ContainerShadow style={style.container}>
      <Text style={style.title}>Demander la reservation de cet appartement</Text>
      <Text style={style.desc}>Apres confirmation de la reservation par l'hote, vous recevrez un lien de paiement par e-mail.</Text>
      <ButtonLoad texte='Envoyer de la demande'/>
    </ContainerShadow>
  );
};
const style = StyleSheet.create({
    container : {
        gap : 10
    },
    title : {
        color : "#000",
        fontSize : 24,
        fontWeight : "bold"
    },
    desc : {
        color : "#000",
        fontSize : 15
    }
})
export default ReservationForm;

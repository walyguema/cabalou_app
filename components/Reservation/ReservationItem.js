import {Image, StyleSheet, Text, View} from 'react-native';
import ContainerShadow from '../ContainerShadow';
import ClicableContainerShadow from '../ClicableContainerShadow';
import Rows from '../Rows';
import { getNumberDays, randomColor } from '../../utils/functions';
import { ASSETS } from '../../utils/assets';
import { formatDate } from '../../utils/variables';

const ReservationItem = ({reservation}) => {
  const start = formatDate(reservation.start_date);
  const end = formatDate(reservation.end_date);
  const {property} = reservation;
  return (
    <ClicableContainerShadow style={style.container}>
      <View style={style.containerImg}>
        <Image source={{uri : property.images[0].path}} style={style.image}/>
      </View>
      <View style={style.containerText}>
        <Text style={style.title}>{property.name}</Text>
        <Text style={{...style.date, ...style.textColor}}>
          {start.day}. {start.date} {start.month}. {start.year} - {end.day}. {end.date} {end.month}. {end.year}
        </Text>
        <Rows>
          <Text style={{...style.textColor}}>Nomb. de nuit</Text>
          <Text style={{...style.textColor}}>{getNumberDays(start.date_string,end.date_string)} nuitees</Text>
        </Rows>
        <Rows>
          <Text style={{...style.textColor}}>Cout de la servation</Text>
          <Text style={{...style.textColor}}>{reservation.total_price.toLocaleString()} XAF</Text>
        </Rows>
        <Rows>
          <Text style={{...style.textColor}}>Reference</Text>
          <Text style={{...style.textColor}}>{reservation.reference}</Text>
        </Rows>
        <Rows>
          <Text style={{...style.textColor}}>Statut</Text>
          <Text style={{...style.statut,backgroundColor : `${reservation.statu.color}`}}>{reservation.statu.label}</Text>
        </Rows>
      </View>
    </ClicableContainerShadow>
  );
};
const style = StyleSheet.create({
  container: {
    gap: 10,
    minHeight: 100,
  },
  containerImg: {
    height : 230
  },
  containerText: {
    flex: 1,
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#000',
    marginBottom : 10
  },
  date: {
    fontSize: 14,
  },
  textColor: {
    color: '#333',
    fontSize : 16
  },
  statut : {
    paddingVertical : 4,
    paddingHorizontal : 10,
    // backgroundColor : randomColor(1),
    color : "#fff",
    borderRadius : 99
  },
  image : {
    width : "100%",
    height : "100%",
    objectFit : "cover",
    borderRadius : 10
  }
});
export default ReservationItem;

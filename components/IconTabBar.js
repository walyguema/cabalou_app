import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { COLOR, screenWidth } from '../utils/variables';

function IconTabBar({focus = false,text = "Reser",image,position="center"}){
    return (
        // <View style={{...style.itemStyle,alignItems : position}}>
          <View style={{alignItems : "center"}}>
            <View style={{...style.contentImage,alignItems : position,width : focus ? screenWidth / 5 : "auto",backgroundColor : focus ? COLOR.TABBARICONS.CONTENERICON.focus : COLOR.TABBARICONS.CONTENERICON.unfocus}}>
            <Image width={50} source={image} style={{...style.iconTab,tintColor : focus ? COLOR.TABBARICONS.ICONS.focus : COLOR.TABBARICONS.ICONS.unfocus}}/>
            </View>
            {/* {text ? <Text style={{...style.label,color : focus ? "royalblue" : "#aaa"}}>{text}</Text> : null} */}
          </View>
        // </View>
    );
}

const style = new StyleSheet.create({
    contentImage : {
        // backgroundColor : "orange",
        paddingVertical : 10,
        paddingHorizontal : 15,
        borderRadius : 10,
        // width : "auto"
        // width : (screenWidth / 5) ,
        // borderRadius : 70,
        // justifyContent : "center",
        // padding : 10,
        // backgroundColor : "orange",
        // marginRight : 3
    },
    iconTab : {
      height : 20,
      width : 20,
      objectFit : "contain",
    //   padding : 10,
    //   backgroundColor : "orange"
      },
    label : {
      marginTop : 0
    },
    header : {
      width : "100%",
      // backgroundColor : "orange",
      padding : 10,
    },
    buttonHeader : {
      width : 50,
      height : 50,
      backgroundColor : "#fff",
      borderRadius : 10,
      elevation : 20,
      shadowColor : "#000"
    }
  });
  
export default IconTabBar;
import React, { useState } from "react";
import { FlatList, Text } from "react-native";
import EmptyData from "./requestRender/EmptyData";
import Loader from "./Loader";

/**
 * 
 * @param {{data : [any],emptyComponent : [React.JSX.Element],renderView : (item : any,index : number,separator : React.JSX.Element | any) => React.JSX.Element}} param0 
 * @returns 
 */
const FlatView = ({data = [],emptyComponent = [],renderView}) => {
    const [view,setView] = useState(8);
    return (
        <>
            <FlatList
            showsVerticalScrollIndicator={false}
            style={{backgroundColor : "#fff",paddingHorizontal : 10}}
            data={data.filter((_,i,__) => i <= view)}
            onEndReachedThreshold={0.4}
            ListEmptyComponent={emptyComponent}
            renderItem={({item,index,sepators}) => {
                if(renderView){
                    return renderView(item,index,sepators);
                }else{
                    return <Text>Add new view</Text>
                }
            }}
            ListFooterComponent={data.length && view >= data.length ? null : data.length ? <Loader/> : null}
            onEndReached={() =>{
                setView(view + 2);
            }}
            />
        </>
    );
}

export default FlatView;
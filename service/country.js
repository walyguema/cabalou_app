// import axios from "axios"
import { axiosInstance } from "../axiosConfig/axiosConfig"

// get the banners data for api
export const getCountry = async () => {
    return axiosInstance.get('/countries')
    .then((response)=>response.data)
    .catch((error)=>console.log(error))
}

export const getCountryForPayment = async () =>{
    return axiosInstance.get('/countries?payment=1')
    .then((response)=>response.data)
    .catch((error)=>console.log(error))
}
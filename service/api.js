import axiosInstance from "../axiosConfig/axiosConfig"

export const getAllProperty = async ({size = 10}) =>{
    return axiosInstance.get(`/properties/all?size=${size}`).then(res =>{
        return res;
      })
}
export const getProperty = async ({property_id}) =>{
    return axiosInstance.get(`/property?property_id=${property_id}`).then(res =>{
        return res;
    })
}
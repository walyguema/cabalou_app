import axiosInstance from "../axiosConfig/axiosConfig"

// get the banners data for api
export const getComments = (id) => {
    return axiosInstance.get('/comments', {params : {property_id: id}}).then((response)=>response)
}
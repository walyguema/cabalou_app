import { axiosInstance } from "../axiosConfig/axiosConfig"

// get the stories data for api
export const getPaymentOptions = () => {
    return axiosInstance.get('/payment-options?begin=0&size=10').then((response)=>response.data)
}

export const makePayment = (data) => {
    return axiosInstance.post('/reservation', data).then((response)=>response).catch((error)=>{
        console.log(error)
    })
}

export const makePaymentConfirm = (data) =>{
    return axiosInstance.post('/pay-reservation', data).then((response)=>response).catch((error)=>{
        console.log(error)
    })
}

export const getReceipt = (id) => {
    return axiosInstance.get('/receipt', {params : {reservation_id : id}}).then((response)=>response)
}

export const checkStatusReservation = (id)=>{
    return axiosInstance.get(`/check-payment-status`, {params : {reservation_id : id}})
    .then((response)=>{
        console.log(response.data)
        return response.data.paid
    })
}

export const getHostPayments = (begin, size) => {
    return axiosInstance.get('/host/payments?begin='+begin+'&size='+size).then((response)=>response.data)
}

export const getPaypoutOperators = (begin, size) => {
    return axiosInstance.get('/host/payout-operators').then((response)=>response.data)
}

export const postPayoutAccount = (data) => {
    const headers = { "Content-Type": "multipart/form-data" };
    return axiosInstance.post(`/host/payout-account`, data, { headers })
    .then((response)=>response)
    .catch((error)=>error)
}

export const getPaypoutAccount = (begin, size) => {
    return axiosInstance.get('/host/payout-accounts').then((response)=>response.data)
}


import axiosInstance from "../axiosConfig/axiosConfig"

// import { RecaptchaVerifier, signInWithPhoneNumber } from "firebase/auth"
// get the banners data for api
export const login = async (password, email) => {
    return axiosInstance.post('/authenticate', {username:email,password:password})
    .then((response)=>response)
}
/**
 * Creation de compte
 * @param {{next_url : string, password : string, email : string, last_name : string, first_name : string, phone_code : string, phone : string, password_confirmation: string, company_name : string, company_number : string, type : "client" | "company"}} credentials 
 * @returns 
 */
export const register = (credentials) => {
    return axiosInstance.post(`/register/${credentials.type}`, credentials).then((response)=>response)
}

export const verifyEmail = (email) => {
    return axiosInstance.post(`/forgot-password`, {email})
}

export const authorize = (refresh_token) => {
    return axiosInstance.post(`/authorize`, {refresh_token})
}
/**
 * Verifie le code envoyé par messagerie
 * @param {String} code code a 6 chiffres
 * @returns {Promise<{statut : boolean,data : any}>}
 */
export const verifiyCodeOtpMessage = (code) => {
    return confirmationResult.confirm(code).then((res) => {return {statut : true,data : res}}).catch((error) => {return {statut : false,data : error}});
}
// export const createRecaptcha = (authConfig,buttonSubmitId) => {
//     if(!window.recaptchaVerifier){
//         window.recaptchaVerifier = new RecaptchaVerifier(authConfig, buttonSubmitId, {
//           'size': 'invisible',
//           'callback': (response) => {
//           //   cette fonction est appeler lors de la soumission du recaptcha
//           }
//         });
//     }
// }
// export const sendOtpMessage = (authConfig,phoneNumber) =>{
//     const recaptcha = window.recaptchaVerifier;
//     return signInWithPhoneNumber(authConfig,phoneNumber,recaptcha)
//     .then(res => {
//         window.confirmationResult = res;
//         return {statut : true,message : "succes"}
//     })
//     .catch(err => ({statut : false, message : err}));
// }
export const sendOtp = (email) => {
    return axiosInstance.post(`/send-email-otp`, {email})
    .then((response)=>response)
    .catch((error)=>error)
}
/**
 * Envoie des otp aux utilisant ayant un numero de telephone ou une adree e-mail
 * @param {{headers : {Lang : "EN" | "FR",Currency : "XAF",type : "EMAIL" | "PHONE"},data : {email : string,phone : string}}} option Donnees de la requete
 * @return {Promise<AxiosResponse>}
 */
export const sendUserOtp = (option) =>{
    console.log("send otp");
    return axiosInstance.post(`/send-otp?type=${option?.headers?.type ?? "EMAIL"}`,{
      email: option?.data.email ?? "",
      phone: option?.data.phone ?? ""
    },{headers : {"Lang" : option?.headers?.Lang ?? "FR","Currency" : option?.headers?.Currency ?? "XAF"}}).then(res => res)
    .catch(err => err)
}
/**
 * Verifie le code reçu par un utilisateur.
 * @param {{headers : {Lang : "EN" | "FR",Currency : "XAF",type : "EMAIL" | "PHONE"},data : {email : string,phone : string,code : string}}} option Donnees de la requete
 * @return {Promise<AxiosResponse>}
 */
export const verifyOtpSendUser = (option) =>{
    console.log("verify otp");
    return axiosInstance.post(`/verify-otp?type=${option?.headers?.type ?? "EMAIL"}`,{
        email: option?.data?.email ?? "",
        phone: option?.data?.phone ?? "",
        code : option?.data?.code ?? ""
    },{headers : {"Lang" : option?.headers?.Lang ?? "FR","Currency" : option?.headers?.Currency ?? "XAF"}}).then(res => res)
    .catch(err => err)
}
/**
 * Verifie le code reçu par un utilisateur.
 * @param {{headers : {Lang : "EN" | "FR",Currency : "XAF"},query : {type : "EMAIL" | "PHONE",value : string}}} option Donnees de la requete
 * @return {Promise<AxiosResponse>}
 */
export const checkAccount = (option) =>{
    return axiosInstance.post(`/check-account?type=${option.query.type}&value=${option.query.value}`,{},{
        headers : {
            "Lang" : option?.headers?.Lang ?? "FR",
            "Currency" : option?.headers?.Currency ?? "XAF"
        }
    }).then(res => res).catch(err => err);
}
export const verifyOtp = (email, code) => {
    return axiosInstance.post(`/verify-email-otp`, {email, code})
    .then((response)=>response)
    .catch((error)=>error)
}

export const setPassword = (old_password, password,password_confirmation) => {
    return axiosInstance.post(`/set-password`, {old_password, password,password_confirmation})
    .then((response)=>response)
    .catch((error)=>error)
}

export const resetPassword = (password, password_confirmation) => {
    return axiosInstance.post(`/reset-password`, {password, password_confirmation})
    .then((response)=>response)
    .catch((error)=>error)
}

export const identity = (data) => {
    const headers = { "Content-Type": "multipart/form-data" };
    return axiosInstance.post(`/identity`, data, { headers })
    .then((response)=>response)
    .catch((error)=>error)
}

export const setProfil = (data)=>{
    return axiosInstance.put(`/update-profile`, data).then((response)=>{
        return response
    }).catch((error)=>error)
}

export const setProfilPicture = (data) => {
    console.log(data)
    const headers = { "Content-Type": "multipart/form-data" };
    return axiosInstance.post(`/set-profile-picture`,  data, { headers })
    .then((response)=>response)
    .catch((error)=>error)
}
/**
 * 
 * @param {{}} data Google datas
 * @returns
 */
export const loginByGoogle = async (data) => {
    try {
        const response = await axiosInstance.post('/social-authenticate', data)
        return (response)
    } catch (error) {
        return (error)
    }
}
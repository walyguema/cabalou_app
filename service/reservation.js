import axiosInstance from "../axiosConfig/axiosConfig"

// get the banners data for api
/**
 * 
 * @param {"previous" | "progress" | "next" | "canceled"} target 
 * @returns 
 */
export const getReservations = async (target) => {
    return axiosInstance.get(`/reservations/${target}`).then((response)=>response)
}

export const getCancellationReasons = () => {
    return axiosInstance.get(`/cancellation-reasons`).then((response)=>response)
}

export const getOneReservation = (id) => {
    return axiosInstance.get(`/reservation`, {params : {reservation_id:id}}).then((response)=>response)
}

export const cancellationReservation = (reservation_id, cancel_reason_id, cancel_reason_text) => {
    return axiosInstance.put(`/cancel-reservation`, {reservation_id,cancel_reason_id,cancel_reason_text }).then((response)=>response)
}

export const changeReservationPeriode = (data) => {
    return axiosInstance.post(`/change-reservation`, data).then((response)=>response)
}

export const getAllReservationStatus = () => {
    return axiosInstance.get(`/reservation-status`).then((response)=>response.data.status)
}

export const getReservationStatus = ()=>{
    return axiosInstance.get(`/reservation-status`).then((response)=>response.data.status)
}

export const getRatingCriterias = ()=>{
    return axiosInstance.get(`/rating-criterias`).then((response)=>response.data.criterias)
}

export const sendComment=(data)=>{
    return axiosInstance.post(`/comment`, data).then((response)=>response)
}

export const saveRating=(data)=>{
    return axiosInstance.post(`/rating`, data).then((response)=>response)
}

export const saveStatus=(data)=>{
    return axiosInstance.get(`/host/reservation/set-status`, {params : data}).then((response)=>response)
}

export const getRefusalReasons = () => {
    return axiosInstance.get(`/host/refusal-reasons`).then((response)=>response)
}

export const saveCheckIn=(data)=>{
    return axiosInstance.post(`/host/reservation/check-in`, data).then((response)=>response)
}

export const saveCheckOut=(data)=>{
    return axiosInstance.post(`/host/reservation/check-out`, data).then((response)=>response)
}

export const saveCheckInClient=(data)=>{
    return axiosInstance.post(`/reservation/check-in`, data).then((response)=>response)
}

export const saveCheckOutClient=(data)=>{
    return axiosInstance.post(`/reservation/check-out`, data).then((response)=>response)
}
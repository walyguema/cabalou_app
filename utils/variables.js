import {Dimensions} from 'react-native';

export const screenWidth = Dimensions.get('window').width;
// Hauteur de l'ecrant
export const screenHeight = Dimensions.get('window').height;

const days = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'];
const month = [
  'Jan',
  'Fév',
  'Mar',
  'Avr',
  'Mai',
  'Juin',
  'Juil',
  'Août',
  'Sep',
  'Oct',
  'Nov',
  'Déc',
];

export const COLOR = {
  TABBARICONS: {
    ICONS: {
      focus: '#fff',
      unfocus: '#aaa',
    },
    CONTENERICON: {
      focus: '#005187',
      unfocus: '#fff',
    },
    // CONTENERICON : {
    //     focus : "#132775",
    //     unfocus : "#f5f5f5"
    // }
  },
  GLOBAL: {
    item: '#6B7A99',
    buttonSec: '#aaa',
    mainColor: 'royalblue',
  },
};
/**
 *
 * @param {string} dateString
 * @returns
 */
export function formatDate(dateString) {
  const date = new Date(dateString);
  return {
    day: days[date.getDay()],
    month: month[date.getMonth()],
    year: date.getFullYear(),
    date: date.getDate(),
    date_string : dateString
  };
}
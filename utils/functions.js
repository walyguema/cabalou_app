import { useDispatch } from "react-redux"
import { loginUser } from "../redux/actions/userSlice";

/**
 * 
 * @param {number} left 
 * @param {number} right 
 * @param {number} bottomLeft 
 * @param {number} bottomRight 
 * @returns 
 */
export const radius = (left = 0,right = 0,bottomLeft = 0, bottomRight = 0) =>{
    return {
        borderTopLeftRadius : left,
        borderTopRightRadius : right,
        borderBottomLeftRadius : bottomLeft,
        borderBottomEndRadius : bottomRight
    }
}
export function authUser(user) {
    const dispatch = useDispatch();
    dispatch(loginUser(user));
}

export function randomColor(opactiy = 1) {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgba(${r},${g},${b},${opactiy})`;
}
export function addShadow(){
    return {
        // backgroundColor: "#fff",
        shadowColor: "#999",
        shadowOffset: {
            width: 0,
            height: 9,
        },
        shadowOpacity: 0.22,
        shadowRadius: 9.22,
        elevation: 12
    };
}
/**
 * Trouve le nombre de jour entre deux date.
 * @param {string} dateStart 
 * @param {string} dateEnd 
 * @returns 
 */
export function getNumberDays(dateStart,dateEnd) { 
    const date1 = new Date(dateStart);
    const date2 = new Date(dateEnd);
    const differenceMs = date2 - date1;
    const differenceDays = Math.floor(differenceMs / (1000 * 60 * 60 * 24));
  
    return differenceDays;
}
/**
 * 
 * @param {[object]} datas 
 * @returns
 */
export function filterArray(datas){
    return datas;
}

export function placeholer(width = 100,height = 100){
    return `https://placehold.jp/919191/ffffff/${width}x${height}.png?text=Cabalou`;
}
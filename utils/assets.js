export const ASSETS = {
    ICONSTAB : {
        home : require("../assets/bottomBar/home.png"),
        reservation : require("../assets/bottomBar/booking.png"),
        message : require("../assets/bottomBar/message.png"),
        favoris : require("../assets/bottomBar/favoris.png"),
        profil : require("../assets/bottomBar/profil.png"),
    },
    SEARSH : {
        recherche : require("../assets/icons/rech.png")
    },
    CATEGORIES : {
        all : require("../assets/icons/all.png"),
        foret : require("../assets/icons/foret.png"),
        immeuble : require("../assets/icons/immeuble.png"),
        luxe : require("../assets/icons/luxieux.png"),
        piscine : require("../assets/icons/picine.png"),
        plage : require("../assets/icons/plage.png")
    },
    PROPERTY : {
        propertyImg : require("../assets/icons/img.png"),
        favoris : require("../assets/icons/favoris_hearth.png")
    },
    HEBERGEUR : {
        hebergeurImg : require("../assets/hébergeur.png"),
    },
    USER : {
        UserImg1 : require("../assets/commentaire1.png"),
        UserImg2 : require("../assets/commentaire2.png"),
    },

    GLOBAL : {
        star : require("../assets/icons/star.png"),
        empty : require("../assets/icons/empty.png"),
        phone : require("../assets/icons/phone.png"),
        mail : require("../assets/icons/mail.png"),
        key : require("../assets/icons/key.png"),
        lock : require("../assets/icons/lock.png"),
        arrow_bottom : require("../assets/icons/arrow_bottom.png"),
        galerie : require("../assets/icons/galerie.png"),
        user_empty : require("../assets/user/user.jpg"),
        confidentialite : require("../assets/profile/conf.png"),
        logout : require("../assets/profile/logout.png"),
        translate : require("../assets/profile/translate.png"),
    },
    SWIPER : {
        step1 : require("../assets/swipe-screen/step-1.jpg"),
        step2 : require("../assets/swipe-screen/step-2.jpg"),
        step3 : require("../assets/swipe-screen/step-3.jpg"),
    }
}
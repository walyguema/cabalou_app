import axios from "axios";
import { store } from "../redux/store";
// https://api.cabalou.net/api/v1
const axiosInstance = axios.create({
    // baseURL : "https://test.api.cabalou.net/api/v1",
    baseURL : "https://api.cabalou.net/api/v1",
    headers : {
        Authorization : "key cyZTdhMGRjNWZiZmZhkmUyMTk1NDc0ZjQ1YyIsInRhZyI6IiJ9btqaeyb=="
    }
})
axiosInstance.interceptors.request.use((config) =>{
    const {accessToken} = store.getState().auth;
    // console.log('====================================');
    // console.log(accessToken);
    // console.log('====================================');
    if(accessToken){
        config.headers.Authorization = `Bearer ${accessToken}`;
    }
    return config;
})
export default axiosInstance;